<?php

defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

class Admin_comment extends ADMIN_Controller {

  	function __construct() {
		parent::__construct ();
        $this->load->model('comment_model');
    }
    function articlereplaycommentlist(){
    	if (empty ( $message ))
    		unset ( $message );
    		if(!isset($_SESSION)){
    			session_start();
    		}
    		@$page = max ( 1, intval ( $this->uri->segment ( 3 ) ) );
    		if($_POST){
    			unset($_SESSION['replayword']);
    			if(isset($_POST['word'])){
    				$_SESSION['replayword']=$_POST['word'];
    				
    			}
    			if(isset($_POST['pagesize'])){
    				$_SESSION['pagesize']=intval($_POST['pagesize'])<=0 ? $this->setting ['list_default']:intval($_POST['pagesize']);
    				
    			}
    		}
    		
    		$pagesize = $this->setting ['list_default'];
    		if(isset($_SESSION['pagesize'])){
    			$pagesize=$_SESSION['pagesize'];
    		}else{
    			$_SESSION['pagesize']=$pagesize;
    		}
    		$startindex = ($page - 1) * $pagesize;
    		
    		$data['start']=$startindex;
    		$data['limit']=$pagesize;
    		
    		
    		if(isset($_SESSION['replayword'])){
    			$data['word']=$_SESSION['replayword'];
    		}
    		$data['type']="article_replay";
    		
    		$commentlist = $this->comment_model->getcommentlist ($data);
    		//echo $this->db->last_query();exit();
    		$navtitle="文章评论中的回复评论管理";
    		$rownum=$this->comment_model->rownumlist ($data);
    		$departstr = page ( $rownum, $pagesize, $page, "admin_comment/articlereplaycommentlist" );
    		
    		include template ( 'comment_articlereplaylist', 'admin' );
    }
    /**
    
    * 公告评论
    
    * @date: 2020年12月4日 下午7:41:40
    
    * @author: 61703
    
    * @param: variable
    
    * @return:
    
    */
    function notecommentlist(){
    	if (empty ( $message ))
    		unset ( $message );
    		if(!isset($_SESSION)){
    			session_start();
    		}
    		@$page = max ( 1, intval ( $this->uri->segment ( 3 ) ) );
    		if($_POST){
    			unset($_SESSION['noteword']);
    			if(isset($_POST['word'])){
    				$_SESSION['noteword']=$_POST['word'];
    				
    			}
    			if(isset($_POST['pagesize'])){
    				$_SESSION['pagesize']=intval($_POST['pagesize'])<=0 ? $this->setting ['list_default']:intval($_POST['pagesize']);
    				
    			}
    		}
    		
    		$pagesize = $this->setting ['list_default'];
    		if(isset($_SESSION['pagesize'])){
    			$pagesize=$_SESSION['pagesize'];
    		}else{
    			$_SESSION['pagesize']=$pagesize;
    		}
    		$startindex = ($page - 1) * $pagesize;
    		
    		$data['start']=$startindex;
    		$data['limit']=$pagesize;
    		
    		
    		if(isset($_SESSION['noteword'])){
    			$data['word']=$_SESSION['noteword'];
    		}
    		$data['type']="note";
    		
    		$commentlist = $this->comment_model->getcommentlist ($data);
    		$navtitle="公告评论管理";
    		$rownum=$this->comment_model->rownumlist ($data);
    		$departstr = page ( $rownum, $pagesize, $page, "admin_comment/notecommentlist" );
    		
    		include template ( 'comment_notelist', 'admin' );
    }
    /**
    
    * 回答评论列表
    
    * @date: 2020年12月4日 下午7:25:21
    
    * @author: 61703
    
    * @param: variable
    
    * @return:
    
    */
    function answercommentlist(){
    	if (empty ( $message ))
    		unset ( $message );
    		if(!isset($_SESSION)){
    			session_start();
    		}
    		@$page = max ( 1, intval ( $this->uri->segment ( 3 ) ) );
    		if($_POST){
    			unset($_SESSION['answerword']);
    			if(isset($_POST['word'])){
    				$_SESSION['answerword']=$_POST['word'];
    				
    			}
    			if(isset($_POST['pagesize'])){
    				$_SESSION['pagesize']=intval($_POST['pagesize'])<=0 ? $this->setting ['list_default']:intval($_POST['pagesize']);
    				
    			}
    		}
    		
    		$pagesize = $this->setting ['list_default'];
    		if(isset($_SESSION['pagesize'])){
    			$pagesize=$_SESSION['pagesize'];
    		}else{
    			$_SESSION['pagesize']=$pagesize;
    		}
    		$startindex = ($page - 1) * $pagesize;
    		
    		$data['start']=$startindex;
    		$data['limit']=$pagesize;
    		
    		
    		if(isset($_SESSION['answerword'])){
    			$data['word']=$_SESSION['answerword'];
    		}
    		$data['type']="answer";
    		
    		$commentlist = $this->comment_model->getcommentlist ($data);
    		//echo $this->db->last_query();exit();
    		$navtitle="回答评论管理";
    		$rownum=$this->comment_model->rownumlist ($data);
    		$departstr = page ( $rownum, $pagesize, $page, "admin_comment/answercommentlist" );
    		
    		include template ( 'comment_answerlist', 'admin' );
    }
    /**
    
    * 文章评论管理
    
    * @date: 2020年12月4日 下午5:59:48
    
    * @author: 61703
    
    * @param: variable
    
    * @return:
    
    */
    function commentarticlelist() {
    	if (empty ( $message ))
    		unset ( $message );
    		if(!isset($_SESSION)){
    			session_start();
    		}
    		@$page = max ( 1, intval ( $this->uri->segment ( 3 ) ) );
    		if($_POST){
    			unset($_SESSION['word']);
    			if(isset($_POST['word'])){
    				$_SESSION['word']=$_POST['word'];
    				
    			}
    			if(isset($_POST['pagesize'])){
    				$_SESSION['pagesize']=intval($_POST['pagesize'])<=0 ? $this->setting ['list_default']:intval($_POST['pagesize']);
    				
    			}
    		}
    		
    		$pagesize = $this->setting ['list_default'];
    		if(isset($_SESSION['pagesize'])){
    			$pagesize=$_SESSION['pagesize'];
    		}else{
    			$_SESSION['pagesize']=$pagesize;
    		}
    		$startindex = ($page - 1) * $pagesize;
    		
    		$data['start']=$startindex;
    		$data['limit']=$pagesize;
    	
    	
    		if(isset($_SESSION['word'])){
    			$data['word']=$_SESSION['word'];
    		}
    		$data['type']="article";
    		
    		$commentlist = $this->comment_model->getcommentlist ($data);
    	
    		$navtitle="文章评论管理";
    		$rownum=$this->comment_model->rownumlist ($data);
    		$departstr = page ( $rownum, $pagesize, $page, "admin_comment/commentarticlelist" );
   
    		include template ( 'comment_articlelist', 'admin' );
     
    }
    /**
    
    * 删除文章评论
    
    * @date: 2020年12月4日 下午6:35:47
    
    * @author: 61703
    
    * @param: variable
    
    * @return:
    
    */
    function deletearticlecomment(){
    	if(!isset($_POST['delete'])){
    		$this->message("请选择需要删除的评论");
    		exit();
    	}
    	if($_POST){
    		foreach ($_POST['delete'] as $commentid){
    			$commentid=intval($commentid);
    			$this->db->where(array('id'=>$commentid))->delete('articlecomment');
    			$this->db->where(array('tid'=>$commentid))->delete('article_comment');
    		}
    		$this->message("删除评论成功");
    	}
    }
    /**
    
    * 删除文章评论回复
    
    * @date: 2020年12月4日 下午7:44:19
    
    * @author: 61703
    
    * @param: variable
    
    * @return:
    
    */
    function deletearticlereplaycomment(){
    	if(!isset($_POST['delete'])){
    		$this->message("请选择需要删除的评论");
    		exit();
    	}
    	if($_POST){
    		foreach ($_POST['delete'] as $commentid){
    			$commentid=intval($commentid);
    			$this->db->where(array('id'=>$commentid))->delete('article_comment');
    		}
    		$this->message("删除评论成功");
    	}
    }
    /**
    
    * 删除回答评论
    
    * @date: 2020年12月4日 下午7:33:26
    
    * @author: 61703
    
    * @param: variable
    
    * @return:
    
    */
    function deleteanswercomment(){
    	if(!isset($_POST['delete'])){
    		$this->message("请选择需要删除的评论");
    		exit();
    	}
    	if($_POST){
    		foreach ($_POST['delete'] as $commentid){
    			$commentid=intval($commentid);
    			$this->db->where(array('id'=>$commentid))->delete('answer_comment');
    		}
    		$this->message("删除评论成功");
    	}
    }
    /**
    
    * 删除公告评论
    
    * @date: 2020年12月4日 下午7:39:22
    
    * @author: 61703
    
    * @param: variable
    
    * @return:
    
    */
    function deletenotecomment(){
    	if(!isset($_POST['delete'])){
    		$this->message("请选择需要删除的评论");
    		exit();
    	}
    	if($_POST){
    		foreach ($_POST['delete'] as $commentid){
    			$commentid=intval($commentid);
    			$this->db->where(array('id'=>$commentid))->delete('note_comment');
    		}
    		$this->message("删除评论成功");
    	}
    }
}
