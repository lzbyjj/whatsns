<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Admin_inform extends ADMIN_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->model ( "inform_model" );
		$this->load->model ( "topic_model" );
		$this->load->model ( "answer_model" );
		$this->load->model ( "question_model" );
		$this->load->model ( "topdata_model" );
	}
	function index($msg = '') {
		$sql = "alter table " .  $this->db->dbprefix  . "inform add COLUMN ischuli int(2)  DEFAULT 0;";
		$this->db->query($sql);
		@$page = max ( 1, intval ( $this->uri->segment ( 3 ) ) );
		$pagesize = $this->setting ['list_default'];
		$startindex = ($page - 1) * $pagesize;
		$informlist = $this->inform_model->get_list ( $startindex, $pagesize );
		$informnum = returnarraynum ( $this->db->query ( getwheresql ( 'inform', '1=1', $this->db->dbprefix ) )->row_array () );
		$departstr = page ( $informnum, $pagesize, $page, "admin_inform/index" );
		$msg && $message = $msg;
		include template ( 'informlist', 'admin' );
	}
	/**
	
	* 删除举报内容--问题，回答，文章
	
	* @date: 2020年12月10日 上午11:41:07
	
	* @author: 61703
	
	* @param: variable
	
	* @return:
	
	*/
	function deletecontent(){
		$id=intval ( $this->uri->segment ( 3 ) ) ;
		if(!$id){
			$this->message("删除内容不存在");
			exit();
		}
		$inform = $this->db->get_where ( 'inform', array (
				'id' => $id
		) )->row_array ();
		if (! $inform) {
			$this->message ( "当前举报不存在，id=$id" );
			exit ();
		}
		if($inform['ischuli']){
			$this->message ( "当前举报已经处理" );
			exit ();
		}
		$qid = $inform ['qid'];
		$aid = $inform ['aid'];
		$tagname = '';
		$title = $inform ['qtitle'];
		if ($qid == 0 && $aid != 0) {
			$tagname = "文章";
	       //判断文章是否存在
	       $topic=$this->db->get_where("topic",array('id'=>$aid))->row_array();
		}
		if ($qid != 0 && $aid != 0) {
		
			$tagname = "回答";
			$answer=$this->db->get_where("answer",array('id'=>$aid))->row_array();
		}
		if ($qid != 0 && $aid == 0) {
		
			$tagname = "问题";
			$question=$this->db->get_where("question",array('id'=>$qid))->row_array();
		}
		// 发送删除私信
		
		$subject = "您对" . $tagname . "【" . $title . "】的举报成功";
		$messagecontent = $subject . "," . "已证实举报".$tagname."不合规，已将".$tagname."删除";
		if (! empty ( $inform ['content'] ) && trim ( $inform ['content'] ) != '') {
			$messagecontent .= "  举报内容:" . $inform ['content'];
		}
		$messagedata = array (
				"from" => "系统管理员",
				"fromuid" => 0,
				"touid" => $inform ['uid'],
				"new" => 1,
				"subject" => $subject,
				"time" => time (),
				"content" => $messagecontent,
				"status" => 0,
				"typename" => "reinform"
				
		);
		$this->db->insert ( "message", $messagedata );
		$msgid = $this->db->insert_id ();
		if ($msgid) {
			if ($qid == 0 && $aid != 0&&$topic) {
				$tagname = "文章";
			
				$this->load->model ( 'topdata_model' );
				$this->topdata_model->remove ( $topic ['id'], 'topic' );
				$uid = $topic ['authorid'];
				$this->load->model ( "doing_model" );
				$this->doing_model->deletedoing ( $uid, 9, $topic ['id'] ); // 删除动态
				
				// 删除文章，扣减财富值
				$touser = $this->user_model->get_by_uid ( $uid );
				$koujiancredit1 = intval ( $this->setting ['credit1_article'] );
				if ($touser ['credit1'] < $koujiancredit1) {
					$koujiancredit1 = $touser ['credit1'] >= 0 ? $touser ['credit1'] : 0;
				}
				
				$koujiancredit2 = intval ( $this->setting ['credit2_article'] );
				if ($touser ['credit2'] < $koujiancredit2) {
					$koujiancredit2 = $touser ['credit2'] >= 0 ? $touser ['credit2'] : 0;
				}
				
				$this->credit ( $uid, - $koujiancredit1, - $koujiancredit2, 0, 'delarticle' );
				
				$this->topic_model->remove ($aid );
			}
			if ($qid != 0 && $aid != 0&&$answer) {

				// 删除回答问题，扣减积分
				$uid = $answer ['authorid'];
				$touser = $this->user_model->get_by_uid ( $uid );
				$koujiancredit1 = intval ( $this->setting ['credit1_answer'] );
				if ($touser ['credit1'] < $koujiancredit1) {
					$koujiancredit1 = $touser ['credit1'] >= 0 ? $touser ['credit1'] : 0;
				}
				
				$koujiancredit2 = intval ( $this->setting ['credit2_answer'] );
				if ($touser ['credit2'] < $koujiancredit2) {
					$koujiancredit2 = $touser ['credit2'] >= 0 ? $touser ['credit2'] : 0;
				}
				
				$this->credit ( $uid, - $koujiancredit1, - $koujiancredit2, 0, 'delanswer' );
				$this->answer_model->remove ( $aid );
			}
			if ($qid != 0 && $aid == 0&&$question) {
			
				// 判断问题是否有悬赏且有回答
			
				// 如果悬赏财富值大于0，收回悬赏财富值
				if ($question ['price'] > 0) {
					$this->credit ( $question ['authorid'], 0, $question ['price'], 0, 'back' );
				}
				// 如果问题设置隐藏，收回扣除隐藏财富值
				if ($question ['hidden'] > 0) {
					$this->credit ( $question ['authorid'], 0, 10, 0, 'hiddenback' );
				}
				// 获取当前提问人记录
				$touser = $this->user_model->get_by_uid ( $question ['authorid'] );
				// 获取发布问题经验值
				$koujiancredit1 = intval ( $this->setting ['credit1_ask'] );
				if ($touser ['credit1'] < $koujiancredit1) {
					$koujiancredit1 = $touser ['credit1'] >= 0 ? $touser ['credit1'] : 0;
				}
				// 获取发布问题财富值
				$koujiancredit2 = intval ( $this->setting ['credit2_ask'] );
				if ($touser ['credit2'] < $koujiancredit2) {
					$koujiancredit2 = $touser ['credit2'] >= 0 ? $touser ['credit2'] : 0;
				}
				// 回收财富值
				$this->credit ( $question ['authorid'], - $koujiancredit1, - $koujiancredit2, 0, 'delquestion' );
				$touser = $this->user_model->get_by_uid ( $question ['authorid'] );
				$credit1 = $touser ['credit1'];
				if ($touser ['credit1'] < 0) {
					$credit1 = 0;
				}
				$credit2 = $touser ['credit2'];
				if ($touser ['credit2'] < 0) {
					$credit2 = 0;
				}
				if ($credit1 == 0 || $credit2 == 0) {
					$this->db->where ( array (
							'uid' => $question ['authorid']
					) )->update ( 'user', array (
							'credit1' => $credit1,
							'credit2' => $credit2
					) );
				}
				$this->question_model->remove ( $qid );
			}
			//更新举报状态		
			$this->db->where(array('id'=>$id))->update("inform",array('ischuli'=>1));
			$this->message ( "处理".$tagname."成功" );
			exit ();
		}
		
	}
	/**
	 *
	 * 删除举报
	 *
	 * @date: 2020年12月10日 上午11:40:33
	 *
	 * @author : 61703
	 *        
	 * @param
	 *        	: variable
	 *        	
	 * @return :
	 *
	 *
	 */
	function remove() {
		if (! $_POST ['qid']) {
			$this->message ( "删除的举报内容不存在" );
			exit ();
		}
		
		foreach ( $this->input->post ( 'qid' ) as $informid ) {
			$id = intval ( $informid );
			$inform = $this->db->get_where ( 'inform', array (
					'id' => $id 
			) )->row_array ();
			if (! $inform) {
				$this->message ( "当前举报不存在，id=$id" );
				exit ();
			}
			$qid = $inform ['qid'];
			$aid = $inform ['aid'];
			$tagname = '';
			$title = $inform ['qtitle'];
			if ($qid == 0 && $aid != 0) {
				$tagname = "文章";
				$url = url ( "topic/getone/$aid" );
				$title = "<a href='$url'>$title</a>";
			}
			if ($qid != 0 && $aid != 0) {
				$url = url ( "question/answer/$qid/$aid" );
				$title = "<a href='$url'>$title</a>";
				$tagname = "回答";
			}
			if ($qid != 0 && $aid == 0) {
				$url = url ( "question/view/$qid" );
				$title = "<a href='$url'>$title</a>";
				$tagname = "问题";
			}
			// 发送删除私信
			
			$subject = "您对" . $tagname . "【" . $title . "】的举报已被删除";
			$messagecontent = $subject . "," . addslashes ( strip_tags ( $_POST ['messagecontent'] ) );
			if (! empty ( $inform ['content'] ) && trim ( $inform ['content'] ) != '') {
				$messagecontent .= "  举报内容:" . $inform ['content'];
			}
			$messagedata = array (
					"from" => "系统管理员",
					"fromuid" => 0,
					"touid" => $inform ['uid'],
					"new" => 1,
					"subject" => $subject,
					"time" => time (),
					"content" => $messagecontent,
					"status" => 0,
					"typename" => "inform" 
			
			);
			$this->db->insert ( "message", $messagedata );
			$msgid = $this->db->insert_id ();
			if ($msgid) {
				// 删除举报
				$this->db->where ( array (
						'id' => $id 
				) )->delete ( "inform" );
			}
		}
		$this->message ( "删除举报成功" );
		exit ();
	}
}
?>