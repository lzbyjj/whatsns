<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Plugin_article extends CI_Controller {
	var $whitelist;
	function __construct() {
		$this->whitelist = "get";
		parent::__construct ();
	}
	function get_http_code($url) {
	    $message=array();
		$header = array(			
				'Cookie: __jsluid_h=9c8a8d9d5e6e170120e93600927229a0; mfw_uuid=5f1548bb-6f5a-fae4-d9df-e23c0f0761dc; uva=s%3A91%3A%22a%3A3%3A%7Bs%3A2%3A%22lt%22%3Bi%3A1595230396%3Bs%3A10%3A%22last_refer%22%3Bs%3A23%3A%22http%3A%2F%2Fwww.mafengwo.cn%2F%22%3Bs%3A5%3A%22rhost%22%3BN%3B%7D%22%3B; __mfwurd=a%3A3%3A%7Bs%3A6%3A%22f_time%22%3Bi%3A1595230396%3Bs%3A9%3A%22f_rdomain%22%3Bs%3A15%3A%22www.mafengwo.cn%22%3Bs%3A6%3A%22f_host%22%3Bs%3A3%3A%22www%22%3B%7D; __mfwuuid=5f1548bb-6f5a-fae4-d9df-e23c0f0761dc; UM_distinctid=1736b241c71cb-0ffe5af3cc5157-5373e62-144000-1736b241c7215f; PHPSESSID=hkje8r0isnrbp9217p45qcrt87; oad_n=a%3A3%3A%7Bs%3A3%3A%22oid%22%3Bi%3A1029%3Bs%3A2%3A%22dm%22%3Bs%3A15%3A%22www.mafengwo.cn%22%3Bs%3A2%3A%22ft%22%3Bs%3A19%3A%222020-08-09+10%3A51%3A02%22%3B%7D; __mfwa=1595230394392.75628.3.1595385795651.1596941461810; __mfwlv=1596941461; __mfwvn=3; CNZZDATA30065558=cnzz_eid%3D827237877-1595228870-http%253A%252F%252Fwww.mafengwo.cn%252F%26ntime%3D1596938118; bottom_ad_status=0; _r=csdn; _rp=a%3A2%3A%7Bs%3A1%3A%22p%22%3Bs%3A47%3A%22blog.csdn.net%2Fstarsliu%2Farticle%2Fdetails%2F49019689%22%3Bs%3A1%3A%22t%22%3Bi%3A1596942720%3B%7D; __mfwothchid=referrer%7Cblog.csdn.net; __omc_chl=; __omc_r=blog.csdn.net; __mfwc=referrer%7Cblog.csdn.net; __mfwlt=1596942720; Hm_lvt_8288b2ed37e5bc9b4c9f7008798d2de0=1595230395,1596941462,1596942720; Hm_lpvt_8288b2ed37e5bc9b4c9f7008798d2de0=1596942720; __jsl_clearance=1596956669.399|0|Yu1dw%2F2CelJ6tbnqCuG6sRkFmOc%3D',				
				'Host: www.mafengwo.cn',
				'Referer: '.$url,
				'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36',
		
				
		);
		$curl = curl_init();
		//设置抓取的url
		curl_setopt($curl, CURLOPT_URL, $url);
		//设置头文件的信息作为数据流输出
		curl_setopt($curl, CURLOPT_HEADER, 0);
		// 超时设置,以秒为单位
		curl_setopt($curl, CURLOPT_TIMEOUT, 1);
				
		// 设置请求头
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		//设置获取的信息以文件流的形式返回，而不是直接输出。
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		//执行命令
		$message['data'] = curl_exec($curl);
		$message['statuscode']=curl_getinfo($curl,CURLINFO_HTTP_CODE);
			curl_close($curl);
			return $message;
	}
	function get() {
		header ( "Content-type: text/html; charset=utf-8" );		
		require FCPATH . 'application/controllers/Admin/simple_html_dom.php';
		$url = 'https://www.jb51.net/article/190691.htm'; // 'https://www.toutiao.com/a6858424642978185736/'; // "https://haiwaituiguang.com/article-92123.html";//"https://news.163.com/20/0807/20/FJF3C670000189FH.html";
		if (! strstr ( $url, 'www.jianshu.com' ) && ! strstr ( $url, 'zhuanlan.zhihu.com' )) {
			ini_set ( 'user_agent', 'Mozilla/4.0 (compatible; MSIE 11.0; Windows NT 6.0; Win64; x64; .NET CLR 2.0.50727; SLCC1; Media Center PC 5.0; .NET CLR 3.0.30618; .NET CLR 3.5.30729)' );
		}

		if(strstr($url,"www.mafengwo.cn")){
			$message= $this->get_http_code($url);
			if($message['statuscode']==200){
				
				$htmldom = new simple_html_dom ();
				$html = $htmldom->load ( $message['data'] );
			}else{
				exit ( "没有抓到内容" );
			}
		}else{
			$html = file_get_html ( $url );
		}
	
		if (empty ( $html )) {
			exit ( "没有抓到内容" );
		}
		//content:(.+)
		//content\s*:\s*'{1}',\s*groupId
	
		$secondId='';
		$playurl='';
		$playvedio='';
		if(strstr ( $url, 'baike.baidu.com' )){
			preg_match("/.{1}secondId.{15}/",$html,$m);
			if($m){
				$secondId=str_replace('"secondId":', '', $m[0]);
			}
			if(strstr($secondId,',')){
				$seclist=explode(',', $secondId);
				$secondId=intval($seclist[0]);
				if($secondId){
					$jsonstr=file_get_contents("https://baike.baidu.com/api/wikisecond/playurl?secondId=$secondId");
					if($jsonstr){
						$json=json_decode($jsonstr,true);
						$playurl=$json['list']['mp4Url'];
						$playvedio='<video id="sl-player-el-video" controls="controls" autoplay="autoplay" width="100%" src="'.$playurl.'"></video>';
						
					}
				}
			}
			
		}
		$titlelist = $html->find ( "title" );
		$title = $titlelist [0]->plaintext;
		if(!$title){
			echo $html;exit();
		}
		
		if(strstr($url,'www.toutiao.com')){
			$htmltoutiao=$html;
			$clist=explode("content:", $htmltoutiao);
			if(is_array($clist)){
				$cstr2= $clist[1];
				$clist2=explode("groupId:", $cstr2);
				if(is_array($clist2)){
					
					$content = preg_replace("/\\\\u([0-9a-f]{3,4})/i", "&#x\\1;", $clist2[0]);
					$content = html_entity_decode($content, null, 'UTF-8');
					
					$content=str_replace("'.slice(6, -6),", '', $content);
					$content=str_replace('\&quot;', '"', $content);
					$content=str_replace("'&quot;", '', $content);
					$content=str_replace("&quot;", '', $content);
					
					
				}
				
			}
			$html->clear ();
			echo $content;exit();
		}
		
		if (strstr ( $url, 'show.gotokeep.com/articles' )) {
			$titlelist = $html->find ( "title" );
			if($titlelist){
				$title = $titlelist [0]->plaintext;
				$title=str_replace('_Keep', '', $title);
			}
			
			if (! $title) {
				echo $html;
				exit ();
			}
			
			
			$message ['title'] = $title;
			$htmltoutiao = $html;
			$clist = explode ( "html:", $htmltoutiao );
			if (is_array ( $clist )) {
				$cstr2 = $clist [1];
				$clist2 = explode ( "viewCount:", $cstr2 );
				if (is_array ( $clist2 )) {
					
					$content = preg_replace ( "/\\\\u([0-9a-f]{3,4})/i", "&#x\\1;", $clist2 [0] );
					$content = html_entity_decode ( $content, null, 'UTF-8' );
					$content = str_replace ( '\"', '', $content );
					$content = substr ( $content, 2 );
					$content = substr ( $content, 0, strlen ( $content ) - 6 );
					$subhtml = new simple_html_dom ();
					$_subhtml = $subhtml->load ( $content );
					
					$content = $this->filttercontent ( $url, $_subhtml, $subhtml );
					
					$articleh1 = $subhtml->find ( "h1" );
					
					if ($articleh1) {
						$articleh1 [0]->outertext = '';
					}
					
					$content = $subhtml->save ();
					$subhtml->clear ();
				}
			}
			$html->clear ();
			$message ['article'] = $content;
			$message ['code'] = 2000;
			return $message;
		}

		if (strstr ( $url, 'gotokeep.com/entries' )) {
			
			$titlelist = $html->find ( "title" );
			$title='';
			if($titlelist){
				$title = $titlelist [0]->plaintext;
				$title=str_replace('_Keep', '', $title);
			}
			
			$message ['title'] = $title;
			
			if (! $title) {
				
				echo $html;
				exit ();
			}
			
			
			
			$htmltoutiao = $html;
			// 开始提取内容文本
			$clist = explode ( 'content: \'"', $htmltoutiao );
			if (is_array ( $clist )) {
				$cstr2 = $clist [1];
				$clist2 = explode ( "};", $cstr2 );
				if (is_array ( $clist2 )) {
					
					$content = preg_replace ( "/\\\\u([0-9a-f]{3,4})/i", "&#x\\1;", $clist2 [0] );
					$content = html_entity_decode ( $content, null, 'UTF-8' );
					$content = str_replace ( '\"', '', $content );
					$content = str_replace ( '"\'', '', $content );
					$content = str_replace ( '\n\n', '<br>', $content );
					
					// $content = substr($content, 2);
					// $content = substr($content, 0,strlen($content)-5);
					$subhtml = new simple_html_dom ();
					$_subhtml = $subhtml->load ( $content );
					
					$content = $this->filttercontent ( $url, $_subhtml, $subhtml );
					
					$articleh1 = $subhtml->find ( "h1" );
					
					if ($articleh1) {
						$articleh1 [0]->outertext = '';
					}
					
					$content = $subhtml->save ();
					$subhtml->clear ();
				}
			}
			// 结束提取内容文本
			
			// 开始提取封面图
			$imagesrc = '';
			$clist = explode ( 'photo:', $htmltoutiao );
			if (is_array ( $clist )) {
				$cstr2 = $clist [1];
				$clist2 = explode ( "videoLength:", $cstr2 );
				if (is_array ( $clist2 )) {
					
					// <img style="max-width:100%;margin-bottom:5px;" src=" " https:="" static1.keepcdn.com="" picture="" 2019="" 09="" 23="" 20="" 22="" 71bb0824fc00e9be59cb95efe666864e3ee286e7_1124x1501.jpg="" '="">
					// $imagesrc = preg_replace ( "/\\\\u([0-9a-f]{3,4})/i", "&#x\\1;", $clist2 [0] );
					$imagesrc = html_entity_decode ( $clist2 [0], null, 'UTF-8' );
					$imagesrc = str_replace ( '\',', '', $imagesrc );
					$imagesrc = str_replace ( "'", '', $imagesrc );
				}
			}
			if ($imagesrc) {
				$message ['image'] = $imagesrc;
			}
			// 结束提取封面图
			
			// 开始提取视频地址
			$playsrc = '';
			$clist = explode ( 'video: \'', $htmltoutiao );
			if (is_array ( $clist )) {
				$cstr2 = $clist [1];
				$clist2 = explode ( "photo:", $cstr2 );
				if (is_array ( $clist2 )) {
					
					$playsrc = html_entity_decode ( $clist2 [0], null, 'UTF-8' );
					$playsrc = str_replace ( '\',', '', $playsrc );
					// $imagesrc = str_replace("'", '', $imagesrc);
				}
			}
			if ($playsrc) {
				$message ['playurl'] = $playsrc;
			}
			// 结束提取封视频地址
			
			$html->clear ();
			echo "内容:".$content;exit();
		}
		$titlelist = array ();
		$h1titlelist = $html->find ( "h1" );
		if ($h1titlelist) {
			$title = trim($h1titlelist [0]->plaintext);
		}
		
		// $titlelist = $html->find ( "title" );
		// $title = $titlelist [0]->plaintext; // 获取文章标题
		
		if (! strstr ( $url, 'www.huangqilin.cn' ) && ! $title) {
			$titlelist = $html->find ( "title" );
		}
	
		if ($titlelist && ! $title) {
			
			$title = $titlelist [0]->plaintext; // 获取文章标题
			
			if (empty ( trim ( $title ) )) {
				$titlelist = $html->find ( "h2" );
				$title = $titlelist [0]->plaintext;
				// activity-name
			} else {
				$titlelist = explode ( '-', $title );
				if (is_array ( $titlelist )) {
					$title = $titlelist [0];
				}
				$titlelist = explode ( '_', $title );
				if (is_array ( $titlelist )) {
					$title = $titlelist [0];
				}
			}
		}
		if (! count ( $titlelist ) && ! $title) {
			$titlelist = $html->find ( "h2" );
			$title = $titlelist [0]->plaintext;
		}
		
		if (! strstr ( $url, 'www.jb51.net' ) && ! strstr ( $url, 'news.163.com' ) && ! strstr ( $url, 'new.qq.com' )) {
		} else {
			$title = iconv ( 'gb2312', 'utf-8', $title );
		}
		
		echo "<h1>$title</h1>";
	
	
		$regular = array (
				"div",
				"article" 
		);
		// $reg="content\s*:\s*'[内容1]',\s*groupId*/i";
		
		if (! strstr ( $url, 'zhihu.com' ) && ! strstr ( $url, 'blog.39gs.com' )) {
			$divlist = $html->find ( $regular [1] ); // view-content
			                                         // echo $divlist[0]->innertext;exit();
			$content = $divlist [0]->innertext; // $this->getcontent($url,$divlist,$html);
			if (! empty ( $content )) {
				$subhtml = new simple_html_dom ();
				$_subhtml = $subhtml->load ( $content );
				
				$content = $this->filttercontent ( $url, $_subhtml, $subhtml );
				
				$articleh1 = $subhtml->find ( "h1" );
				
				if ($articleh1) {
					$articleh1 [0]->outertext = '';
				}
				
				$content = $subhtml->save ();
				$subhtml->clear ();
			}
		}
		
		if (empty ( $content )) {
			
			$divlist = $html->find ( $regular [0] );
			$content = $this->getcontent ( $url, $divlist, $html );
		}
		echo '<meta name="referrer" content="no-referrer"/>';
		if (strstr ( $url, 'jingyan.baidu.com' )) {
			echo '<link rel="stylesheet" type="text/css" href="//exp.bdstatic.com/static/exp-pc/article/pkg/article_sync_css_0_e09e946.css"/>';
			echo '<link rel="stylesheet" type="text/css" href="//exp.bdstatic.com/static/exp-pc/common-jquery/pkg/common_5379e37.css"/>';
		}
		echo $playvedio;
		$content = preg_replace ( "/<!--[^\!\[]*?(?<!\/\/)-->/", "", $content );
		echo $content;
		if($message['statuscode']==200){
			$htmldom->clear ();
		}
		$html->clear ();
		exit ();
	}
	function getcontent($url, $divlist, $html) {
		$content = '';
		foreach ( $divlist as $div ) {
			$subtag = $div->first_child ()->tag;
			$thisid = $div->id;
			$thisclass = $div->class;
			if ($thisid == 'rich_media_content ') {
				$div->style = "";
			}
			if ($thisclass == 'cont') {
				$div->first_child ()->innertext = '';
			}
			
			if ($thisclass == 'dialogBox' ||$thisclass == 'fully_loading' || strstr ( $url, 'www.woshipm.com' ) && $subtag == 'h2' || strstr ( $url, 'www.woshipm.com' ) && $thisclass == 'main-content' || $thisclass == '_2MNh6M5nnD1l5EcpLQGupB' || $thisclass == 'pd15-1' || strstr ( $thisclass, 'header' ) || strstr ( $div->parent ()->class, 'aw-dropdown' ) || strstr ( $thisid, 'top-nav-appintro' ) || $thisclass == 'info' || strstr ( $thisclass, 'news_about' ) || strstr ( $thisclass, 'nav1' ) || strstr ( $thisclass, 'post_time' ) || strstr ( $thisclass, 'info_btn2' ) || strstr ( $thisclass, 'aside' ) || strstr ( $thisid, 'aside' )) {
				continue;
			}
			
			if ($thisclass == "article-intro"||$thisclass == "_j_content_box" || $thisclass == "article--content grap" || strstr ( $url, 'www.ali213.net' ) && $thisid == 'Content' || $thisclass == 'sideL' || $thisid == "textContBox" || $thisclass == 'main-content' || $thisid == 'sitetext1' || $thisclass == 'info-con' || $thisclass == 'mp-content' || $thisclass == 'cont' || $thisid == 'txt' || $thisclass == 'pic-content' || $thisid == 'cnblogs_post_body' || $thisclass == 'exp-content-body' || $thisclass == 'topic-content' || $thisclass == 'rich_media_content' || $thisid == 'article' || strstr ( $thisclass, 'article_show_body' ) || strstr ( $thisclass, 'Custom_UnionStyle' ) || strstr ( $thisclass, 'd_img' ) || strstr ( $thisid, 'vjs_video_3' ) || strstr ( $thisclass, 'wb_nr' ) || strstr ( $thisclass, 'news_txt' ) || strstr ( $thisid, 'articleBody' ) || strstr ( $thisclass, 'content-article' ) || strstr ( $thisid, 'artibody' ) || strstr ( $thisid, 'conCon' ) || strstr ( $thisclass, 'post_text' ) || strstr ( $thisid, 'text_content' ) || strstr ( $thisclass, 'content-font' ) || $subtag == "p" || $subtag == "h3" || $subtag == "h2" || strstr ( $thisclass, 'articalContent' )) {
				$subhtmlcontent = "";
				
				if (! strstr ( $url, 'www.jb51.net' ) && ! strstr ( $url, 'news.163.com' ) && ! strstr ( $url, 'new.qq.com' )) {
					$subhtmlcontent = $div->innertext;
				} else {
					$subhtmlcontent = iconv ( 'gb2312', 'utf-8', $div->innertext );
				}
				
				$subhtml = new simple_html_dom ();
				$_subhtml = $subhtml->load ( $subhtmlcontent );
				$tmptitle = $_subhtml->find ( ".articleTitle" );
				if ($tmptitle) {
					$tmptitle [0]->innertext = '';
				}
				$tmpinfo = $_subhtml->find ( ".articleInfo" );
				if ($tmpinfo) {
					$tmpinfo [0]->innertext = '';
				}
				
				$tmpvf = $_subhtml->find ( "#vf" );
				if ($tmpvf) {
					$tmpvf [0]->innertext = '';
				}
				
				$plist = $_subhtml->find ( 'p' );
				
				if (count ( $plist ) > 1||$thisclass == "article-intro" || $thisclass == "_j_content_box" || $thisclass == "article--content grap" || strstr ( $url, 'www.ali213.net' ) && $thisid == 'Content' || $thisclass == 'sideL' || $thisid == "textContBox" || $thisclass == 'main-content' || $thisid == 'sitetext1' || $thisclass == 'info-con' || $thisclass == 'mp-content' || $thisclass == 'cont' || $thisid == 'txt' || $thisclass == 'pic-content' || $thisid == 'cnblogs_post_body' || $thisclass == 'exp-content-body' || $thisclass == 'topic-content' || $thisclass == 'rich_media_content' || $thisid == 'article' || strstr ( $thisclass, 'article_show_body' ) || strstr ( $thisclass, 'Custom_UnionStyle' ) || strstr ( $thisclass, 'd_img' ) || strstr ( $thisid, 'vjs_video_3' ) || strstr ( $thisclass, 'wb_nr' ) || strstr ( $thisclass, 'news_txt' ) || strstr ( $thisid, 'articleBody' ) || strstr ( $thisclass, 'content-article' ) || strstr ( $thisid, 'artibody' ) || strstr ( $thisid, 'conCon' ) || strstr ( $thisclass, 'post_text' ) || strstr ( $thisid, 'text_content' ) || strstr ( $thisclass, 'content-font' )) {
					$subhtmlcontent = $this->filttercontent ( $url, $_subhtml, $subhtml );
					$content = $subhtmlcontent;
					if (! empty ( $content )) {
						break;
					}
				}
				
				$subhtml->clear ();
			}
		}
		
		return $content;
	}
	function filttercontent($url, $_subhtml, $subhtml) {
		$subhtmlcontent = '';
		
		$imglist = $_subhtml->find ( 'img' );
		foreach ( $imglist as $img ) {
			// echo "src=".$img->src;
			$attrs = $img->getAllAttributes ();
			foreach ( $attrs as $attr_key => $attr_value ) {
				if (strstr ( $url, 'www.qvlog.com' )) {
					if ($attr_key == "data-original") {
						if (! strstr ( $attr_value, 'http' )) {
							$img->outertext = "<img style='max-width:100%;margin-bottom: 5px;' src='http://www.qvlog.com" . "$attr_value' />";
						} else {
							if(! strstr ( $attr_value, 'pcload.jpg' )){
								$img->outertext = "<img style='max-width:100%;margin-bottom: 5px;' src='$attr_value' />";
							}
							
						}
					}
				} else {
					if ($attr_key == "data-original") {
						
						$img->outertext = "<img style='max-width:100%;margin-bottom: 5px;' src='$attr_value' />";
					}
					if ($attr_key == "data-original-src") {
						
						$img->outertext = "<img style='max-width:100%;margin-bottom: 5px;' src='$attr_value' />";
					}
					
					if ($attr_key == "data-actualsrc") {
						
						$img->outertext = "<img style='max-width:100%;margin-bottom: 5px;' src='$attr_value' />";
					}
					if ($attr_key == "data-src") {
						
						$img->outertext = "<img style='max-width:100%;margin-bottom: 5px;' src='$attr_value' />";
					}
					if ($attr_key == "real_src") {
						
						$img->outertext = "<img style='max-width:100%;margin-bottom: 5px;' src='$attr_value' />";
					}
					if (strstr ( $url, 'www.guangyuanol.cn' )) {
						if ($attr_key == "src") {
							
							$img->outertext = "<img style='max-width:100%;margin-bottom: 5px;' src='http://www.guangyuanol.cn" . "$attr_value' />";
						}
					}
					
					if ($attr_key == "src"&&!strstr ( $url, 'www.qvlog.com' )&&! strstr ( $attr_value, 'pcload.jpg' )) {
						$litterimgclass = $img->class;
						if ($litterimgclass == 'portraitimg') {
							
							$img->style = "width:80px;height:80px;";
						} else {
							if (! strstr ( $attr_value, 'http' ) && ! strstr ( $attr_value, '.com' ) && ! strstr ( $attr_value, '.cn' )) {
								$_url = strtolower ( $url ); // 首先转成小写
								$hosts = parse_url ( $url );
								$host = $hosts ['host'];
								$img->outertext = "<img style='max-width:100%;margin-bottom: 5px;' src='http://" . $host . $attr_value . "' />";
							} else {
								$img->outertext = "<img style='max-width:100%;margin-bottom: 5px;' src='$attr_value' />";
							}
						}
					}
				}
				
				
			}
		}
		
		$classlist = array (
				".am-article-hd",
				"#info_around",
				".author-info",
				".post-navigation",
				".new_ds",
				".l-topic",
				".sub-tit",
				".p-section",
				".toggle",
				".image-caption",
				"._2UOoDXKoJpb79VHUN4HYLK",
				".sup--normal",
				".caption",
				".article-vote",
				".article-statement",
				".hotspotminingContent_web",
				".go-auth-box",
				".album-list",
				".title-prefix",
				".taglist-inline",
				".post-opt",
				".readmore",
				".dlp",
				".erphpdown-content-vip",
				".erphpdown",
				".article-aside",
				".art_xg",
				".info",
				".title",
				".fl_popup_bottom",
				".weibo_info",
				".lemma-catalog",
				".top-tool",
				".lemmaWgt-lemmaTitle",
				".j-edit-link"
		);
		foreach ( $classlist as $classv ) {
			$opclass = $_subhtml->find ( $classv );
			
			if ($classv == ".j-edit-link" || $classv == ".title-prefix" || $classv == ".sup--normal" || $classv == ".image-caption") {
				foreach ( $opclass as $opclassval ) {
					$opclassval->outertext = '';
				}
			} else {
				if ($opclass) {
					$opclass [0]->outertext = '';
				}
			}
		}
		// para-title
		$paratitle = $_subhtml->find ( ".para-title" );
		foreach ( $paratitle as $paratitleval ) {
			$paratitleval->style = 'display: block;
    clear: both;
    line-height: 24px;
    font-size: 15px;
    font-weight: 400;
    font-family: Microsoft YaHei,SimHei,Verdana;
    position: relative;';
			$paratitleval->outertext = str_replace ( "编辑", "", $paratitleval->outertext );
		}
		$idlist = array (
				"#comments",
				"#tashuo_bottom",
				"#title"
		);
		foreach ( $idlist as $idv ) {
			$opid = $_subhtml->find ( $idv );
			
			if ($opid) {
				$opid [0]->outertext = '';
			}
		}
		
		$classattrlist = array (
				"clearfix appendQr_wrap",
				"text-center mt-30 mb-20",
				"xgcomm clearfix",
				"tags clearfix",
				"weibo_info look_info",
				"lemmaWgt-lemmaTitle lemmaWgt-lemmaTitle-",
				"caption-sub cmn-clearfix"
				
		);
		foreach ( $classattrlist as $classv ) {
			$opclass = $_subhtml->find ( "div[class=$classv]" );
			
			if ($opclass) {
				$opclass [0]->outertext = '';
			}
		}
		
		$header = $_subhtml->find ( "header" );
		$footer = $_subhtml->find ( "footer" );
		if ($header) {
			$header [0]->outertext = '';
		}
		if ($footer) {
			$footer [0]->outertext = '';
		}
		$relpostthumbwrapper = $_subhtml->find ( ".relpost-thumb-wrapper" );
		
		if ($relpostthumbwrapper) {
			$relpostthumbwrapper [0]->outertext = '';
		}
		$picvideo = $_subhtml->find ( ".pic-video" );
		
		if ($picvideo) {
			$picvideo [0]->outertext = '';
		}
		$basicinfo = $_subhtml->find ( ".basic-info" );
		
		if ($basicinfo) {
			$basicinfo [0]->style = 'clear:both';
		}
		$pic_tag = $_subhtml->find ( ".pic_tag" );
		
		if ($pic_tag) {
			foreach ( $pic_tag as $childpic_tag )
				$childpic_tag->style = 'display:block;color: #999;font-size: 12px;';
		}
		
		$divinfo = $_subhtml->find ( "div" );
		
		if ($divinfo) {
			foreach ( $divinfo as $tagdiv ) {
				// para
				$tagclass = $tagdiv->class;
				if ($tagclass == 'para') {
					$tagdiv->style = "font-size: 16px;
    line-height: 32px;
    color: rgba(0,0,0,.8);
    word-break: break-all;";
				} else {
					$tagdiv->style = "";
				}
				if ($tagclass == "detailitem") {
					
					$tagdiv->style = "display: inline-block;
";
				}
			}
		}
		
		$summary = $_subhtml->find ( ".summary" );
		if ($summary) {
			foreach ( $summary as $tagsummary ) {
				$tagsummary->style = "margin: 20px 0;
    border-left: 10px solid #f5f5f5;
    padding-left: 16px;
    color: rgba(0,0,0,.5);";
			}
		}
		$blockquote = $_subhtml->find ( "blockquote" );
		
		if ($blockquote) {
			foreach ( $blockquote as $tagblockquote ) {
				$tagblockquote->style = "margin: 20px 0;
    border-left: 10px solid #f5f5f5;
    padding-left: 16px;
    color: rgba(0,0,0,.5);";
			}
		}
		
		$detailsconother = $_subhtml->find ( ".details-con-other" );
		
		if ($detailsconother) {
			$detailsconother [0]->outertext = '';
		}
		
		$albumwrap = $_subhtml->find ( ".album-wrap" );
		
		if ($albumwrap) {
			$albumwrap [0]->style = 'width:149px;height:220px;float: right;';
		}
		$hotspotminingContent_pic = $_subhtml->find ( ".hotspotminingContent_pic" );
		
		if ($hotspotminingContent_pic) {
			$hotspotminingContent_pic [0]->style = 'float: left;    margin-right: 15px;';
			$fimg = $hotspotminingContent_pic [0]->find ( "img" );
			if ($fimg) {
				$fimg [0]->style = "display: block;
    width: 180px;
    height: 140px;";
			}
		}
		
		$basicInfoitem = $_subhtml->find ( ".basicInfo-item" );
		
		if ($basicInfoitem) {
			foreach ( $basicInfoitem as $binfo ) {
				$binfo->style = 'width: 90px;
    padding: 0 5px 0 12px;
font: 12px/1.5 arial,sans-serif;
    font-weight: 700;
    text-overflow: ellipsis;
    white-space: nowrap;
    word-wrap: normal;
    color: #999;line-height: 26px;
    display: block;
margin: 0;
    ';
			}
		}
		
		$subtitle = $_subhtml->find ( ".subtitle" );
		
		if ($subtitle) {
			$subtitle [0]->outertext = '';
		}
		
		$entryheader = $_subhtml->find ( ".entry-header" );
		
		if ($entryheader) {
			$entryheader [0]->outertext = '';
		}
		$entrymeta = $_subhtml->find ( ".post-views" );
		
		if ($entrymeta) {
			$entrymeta [0]->outertext = '';
		}
		
		$wxtitle = $_subhtml->find ( ".rich_media_title" );
		
		if ($wxtitle) {
			$wxtitle [0]->outertext = '';
		}
		$post_tags = $_subhtml->find ( ".post_tags" );
		
		if ($post_tags) {
			$post_tags [0]->outertext = '';
		}
		$post_info = $_subhtml->find ( ".post_info" );
		
		if ($post_info) {
			$post_info [0]->outertext = '';
		}
		
		$cnbolg = $_subhtml->find ( "h3[id=反馈与建议]" );
		if ($cnbolg) {
			$cnbolg [0]->next_sibling ()->innertext = '';
			$cnbolg [0]->outertext = '';
		}
		$sourseqihangsoft = $_subhtml->find ( ".source" );
		if ($sourseqihangsoft) {
			$sourseqihangsoft [0]->next_sibling ()->innertext = '';
			$sourseqihangsoft [0]->outertext = '';
		}
		
		$entrytitle = $_subhtml->find ( ".entry-title" );
		
		if ($entrytitle) {
			$entrytitle [0]->outertext = '';
		}
		$entrymeta = $_subhtml->find ( ".entry-meta" );
		
		if ($entrymeta) {
			$entrymeta [0]->outertext = '';
		}
		$contTab = $_subhtml->find ( "#contTab" );
		
		if ($contTab) {
			$contTab [0]->outertext = '';
		}
		$contentMore = $_subhtml->find ( "#contentMore" );
		
		if ($contentMore) {
			$contentMore [0]->outertext = '';
		}
		$relatedposts = $_subhtml->find ( ".relatedposts" );
		
		if ($relatedposts) {
			$relatedposts [0]->outertext = '';
		}
		$entryfooter = $_subhtml->find ( ".entry-footer" );
		
		if ($entryfooter) {
			$entryfooter [0]->outertext = '';
		}
		$entrydetails = $_subhtml->find ( ".entry-details" );
		
		if ($entrydetails) {
			$entrydetails [0]->outertext = '';
		}
		
		$bdjy = $_subhtml->find ( ".wgt-feeds-video" );
		
		if ($bdjy) {
			$bdjy [0]->outertext = '';
		}
		$wgtthumbs = $_subhtml->find ( ".wgt-thumbs" );
		
		if ($wgtthumbs) {
			$wgtthumbs [0]->outertext = '';
		}
		$audiowp = $_subhtml->find ( ".audio-wp" );
		
		if ($audiowp) {
			$audiowp [0]->outertext = '';
		}
		$prompt = $_subhtml->find ( ".prompt" );
		
		if ($prompt) {
			$prompt [0]->outertext = '';
		}
		$imagecontainerfill = $_subhtml->find ( ".image-container-fill" );
		
		if ($imagecontainerfill) {
			foreach ( $imagecontainerfill as $imgfill ) {
				$imgfill->outertext = '';
			}
		}
		
		$contentlistblockimage = $_subhtml->find ( ".content-listblock-image" );
		
		if ($contentlistblockimage) {
			$contentlistblockimage [0]->style = 'padding-left:25%;';
		}
		
		$exptitle = $_subhtml->find ( ".exp-title" );
		
		if ($exptitle) {
			$exptitle [0]->outertext = '';
		}
		
		$wgtcomments = $_subhtml->find ( ".wgt-comments" );
		
		if ($wgtcomments) {
			$wgtcomments [0]->outertext = '';
		}
		$originnotice = $_subhtml->find ( ".origin-notice" );
		
		if ($originnotice) {
			$originnotice [0]->outertext = '';
		}
		$readwhole = $_subhtml->find ( ".read-whole" );
		
		if ($readwhole) {
			$readwhole [0]->outertext = '';
		}
		
		$wxinfo = $_subhtml->find ( ".rich_media_meta_list" );
		
		if ($wxinfo) {
			$wxinfo [0]->outertext = '';
		}
		$wxcontent = $_subhtml->find ( ".rich_media_content" );
		
		if ($wxcontent) {
			$wxcontent [0]->style = '';
		}
		$expcontentcontainer = $_subhtml->find ( ".exp-content-container" );
		
		if ($expcontentcontainer) {
			$expcontentcontainer [0]->style = 'overflow:visible;';
		}
		
		$doubanid = $_subhtml->find ( "#link-report_group" );
		
		if ($doubanid) {
			$doubanid [0]->outertext = '';
		}
		
		$tmpcndouban = $_subhtml->find ( ".topic-doc>h3" );
		
		if ($tmpcndouban) {
			$tmpcndouban [0]->outertext = '';
		}
		
		$tmpcnphp = $_subhtml->find ( ".post-title" );
		
		if ($tmpcnphp) {
			$tmpcnphp [0]->outertext = '';
		}
		
		$scriptlist = $_subhtml->find ( 'script' );
		foreach ( $scriptlist as $script ) {
			$script->outertext = "";
		}
		
		$sectiontlist = $_subhtml->find ( 'section' );
		foreach ( $sectiontlist as $section ) {
			// $section->style = "";
		}
		
		$noscriptlist = $_subhtml->find ( 'noscript' );
		foreach ( $noscriptlist as $noscript ) {
			$noscript->outertext = "";
		}
		if (! strstr ( $url, 'jingyan.baidu.com' )) {
			$linklist = $_subhtml->find ( 'a' );
			foreach ( $linklist as $link ) {
				
				$linkimg = $link->find ( "img" );
				if ($linkimg) {
					$link->outertext = $link->innertext;
				} else {
					$link->outertext = $link->plaintext;
				}
			}
		}
		
		$pplist = $_subhtml->find ( 'p' );
		foreach ( $pplist as $pp ) {
			if ($pp->style == 'color:#ff0000') {
				$pp->outertext = "";
			}
			if ($pp->class == "post-byline") {
				$pp->outertext = "";
			}
			
			$ptext = trim ( $pp->plaintext );
			if (empty ( $ptext ) || $ptext == "<br>" || ! $ptext) {
				// $pp->outertext = "";
			}
			$pp->style = "font-size: 16px;
    line-height: 32px;
    color: rgba(0,0,0,.8);
    word-break: break-all;";
			
			if (strstr ( $pp->plaintext, "[广告]赞助链接" )) {
				$pp->next_sibling ()->outertext = '';
				$pp->outertext = "";
			}
			
			if (strstr ( $url, 'www.woshipm.com' ) && strstr ( $pp->plaintext, "作者：" ) || strstr ( $pp->plaintext, "原创发布于人人都是产品经理" ) || strstr ( $pp->plaintext, "题图来自" ) || strstr ( $pp->plaintext, "编者按：" ) || strstr ( $pp->plaintext, "原文：" ) || strstr ( $pp->plaintext, "来源：" ) || strstr ( $pp->plaintext, "责任编辑：" ) || strstr ( $pp->plaintext, "投稿邮箱" )) {
				$pp->outertext = "";
			}
			
			if (strstr ( $pp->plaintext, "返回搜狐，查看更多" )) {
				$pp->outertext = str_replace ( '返回搜狐，查看更多', '', $pp->outertext );
			}
		}
		$subhtmlcontent = $subhtml->save ();
		return $subhtmlcontent;
	}
}