<?php
class Comment_model extends CI_Model {
    var $dbcms;
    function __construct() {
    	$this->load->database ();
    }
    /**
    
    * 获取评论列表--公告，问答，文章
    
    * @date: 2020年12月4日 下午4:30:38
    
    * @author: 61703
    
    * @param: variable
    
    * @return:
    
    */
     function getcommentlist($data){
     	$where='';
     	$word='';
        $tbname='';
        $sql='';
        $limit=isset($data['limit']) ? intval($data['limit']):20;
        $start=isset($data['start']) ? intval($data['start']):0;
        if($data['word']!=''&&!empty($data['word'])){
        	$word=addslashes($data['word']);
        	
        }
     	switch ($data['type']){
     		case 'note':
     			$tbname="note_comment";//公告评论
     			if($word!=''){
     				$where=" and a.content like '%$word%' ";
     			}
     			$sql="select a.id,at.id as urlid,at.title,a.content,a.author,a.authorid,a.time from " . $this->db->dbprefix . "note_comment as a," . $this->db->dbprefix . "note as at where  a.noteid=at.id $where order by a.time desc limit $start, $limit";
     			
     			break;
     		case 'answer':
     			$tbname="answer_comment";//回答评论
     			if($word!=''){
     				$where=" and ac.content like '%$word%' ";
     			}
     			$sql="select a.id as urlid ,a.qid,a.title,a.content as atcontent,ac.content,ac.author,ac.authorid,ac.time,ac.id from " . $this->db->dbprefix . "answer_comment as ac," . $this->db->dbprefix . "answer as a where  a.id=ac.aid $where order by ac.time desc limit $start, $limit";
     			
     			break;
     		case 'article':
     			$tbname="articlecomment";//文章评论
     			if($word!=''){
     				$where=" where content like '%$word%' ";
     			}
     			$sql="select id,tid as urlid,title,content,authorid,author,time from " . $this->db->dbprefix . "articlecomment $where order by time desc  limit $start, $limit";
     			break;
     		case 'article_replay':
     			$tbname="article_comment";//文章评论回复
     			if($word!=''){
     				$where=" and a.content like '%$word%' ";
     			}
     			$sql="select a.id,at.tid as urlid,at.title,at.content as atcontent,a.content,a.author,a.authorid,a.time from " . $this->db->dbprefix . "articlecomment as at," . $this->db->dbprefix . "article_comment as a where  a.aid=at.id $where order by a.time desc limit $start, $limit";
     			
     			break;
     	}
     $commentlist=array();
     	$query=$this->db->query($sql);
     	if($query){
     		foreach ($query->result_array() as $comment){
     			$comment['addtime']=date('Y-m-d H:i:s',$comment['time']);
     		
     			switch ($data['type']){
     				case 'node':
     					$comment['url']=url("note/view/".$comment['urlid']);
     					break;
     				case 'answer':
     					$comment['url']=url("question/answer/".$comment['qid']."/".$comment['urlid']);
     					break;
     				case 'article':
     					$comment['url']=url("topic/getone/".$comment['urlid']);
     					break;
     				case 'article_replay':
     					$comment['url']=url("topic/getone/".$comment['urlid']);
     					break;
     			}
     			$commentlist[]=$comment;
     			
     		}
     	}
     
     	return  $commentlist;
     }
     /**
     
     * 用户评论数量
     
     * @date: 2020年12月4日 下午7:07:28
     
     * @author: 61703
     
     * @param: variable
     
     * @return:
     
     */
     function rownumlist($data){
     	$where='';
     	$word='';
     	$tbname='';
     	$sql='';
     	$limit=isset($data['limit']) ? intval($data['limit']):20;
     	$start=isset($data['start']) ? intval($data['start']):0;
     	if($data['word']!=''&&!empty($data['word'])){
     		$word=addslashes($data['word']);
     		
     	}
     	switch ($data['type']){
     		case 'note':
     			$tbname="note_comment";//公告评论
     			if($word!=''){
     				$where=" and a.content like '%$word%' ";
     			}
     			$sql="select count(a.id) num from " . $this->db->dbprefix . "note_comment as a," . $this->db->dbprefix . "note as at where  a.noteid=at.id $where order by a.time desc ";
     			
     			break;
     		case 'answer':
     			$tbname="answer_comment";//回答评论
     			if($word!=''){
     				$where=" and ac.content like '%$word%' ";
     			}
     			$sql="select count(ac.id) num from " . $this->db->dbprefix . "answer_comment as ac," . $this->db->dbprefix . "answer as a where  a.id=ac.aid $where order by ac.time desc ";
     			
     			break;
     		case 'article':
     			$tbname="articlecomment";//文章评论
     			if($word!=''){
     				$where=" where content like '%$word%' ";
     			}
     			$sql="select count(id) num from " . $this->db->dbprefix . "articlecomment $where order by time desc ";
     			break;
     		case 'article_replay':
     			$tbname="article_comment";//文章评论回复
     			if($word!=''){
     				$where=" and a.content like '%$word%' ";
     			}
     			$sql="select count(a.id) num from " . $this->db->dbprefix . "articlecomment as at," . $this->db->dbprefix . "article_comment as a where  a.aid=at.id $where  order by a.time desc ";
     			
     			break;
     	}
     	$query=$this->db->query($sql);
     	if($query){
     		$rownum=$query->row_array();
     		return $rownum['num'];
     	}else{
     		return 0;
     	}
     }
}
