<!--{template header}-->
<style>

.main-wrapper {
	margin-bottom: 40px;
	background: #fafafa;
	margin-top: 0px;
}

.signature {
	color: #999;
}

.imgjiesu {
	position: absolute;
	right: 10px;
	top: 13px;
	width: 45px;
	height: 35px;
}

.jinxingzhong {
	position: absolute;
	right: 15px;
	top: 13px;
	width: 35px;
	height: 35px;
}

.details-contitle-box {
	position: relative;
}
</style>
<!-- cdn 问题详情顶部固定 -->
<div class="cdn_question_fixedtitle"></div>
<link rel="stylesheet" media="all"	href="{SITE_URL}static/css/common/commtag.css" />
<!--{eval $adlist = $this->fromcache("adlist");}-->
<link rel="stylesheet" media="all"	href="{SITE_URL}static/css/widescreen/css/greendetail.css?v1.1" />
<link rel="stylesheet" media="all"	href="{SITE_URL}static/css/widescreen/css/widescreendetail.css?v1.1" />

<div class="work-details-wrap border-bottom">
<div class="question_insertcode" id="question_insertcode_1"></div>
	<div class="work-details-box-wrap container">
	<div class="question_insertcode" id="question_insertcode_2"></div>
 <!-- 问题相关标签 -->
			{template solve_question_tag}
	<div class="question_insertcode" id="question_insertcode_3"></div>
		<div class="left-details-head col-md-16">
		<div class="question_insertcode" id="question_insertcode_4"></div>
			<div class="details-contitle-box">
				 <!-- 问题标题和标记-->
			{template solve_question_title}
				<div class="question_insertcode" id="question_insertcode_5"></div>
				 <!-- 问题分享和时间-->
			{template solve_question_share}
			 <!-- 问题回答数，收藏数，浏览数参数提示 -->
			{template solve_question_meta}
				<div class="question_insertcode" id="question_insertcode_6"></div>
			</div>
		</div>
		<div class="top-author-card follow-box col-md-8">
			 <!-- cdn节点  提问者信息 -->
			 <div class="cdn_question_userinfo"></div>
		</div>

	</div>
		<div class="question_insertcode" id="question_insertcode_7"></div>
	<div class="container ">

		<div class="content-wrap">
			<div class="question_insertcode" id="question_insertcode_8"></div>
	  <!-- 问题描述-->
		{template solve_question_description}
		<div class="question_insertcode" id="question_insertcode_9" style="clear: both;"></div>
			<div class="details-con-other border-top" {if !$question['description']} style="margin-top: 0px;"{/if}>
				<div class="">
						 
					<div class="three-link">
			   <!-- cdn节点 问题操作按钮 -->
                        <div class="cdn_question_button"></div>
					</div>
				</div>
						
				   <!-- 问题关闭提示 -->
		{template solve_guanbiguize}
			<div class="question_insertcode" id="question_insertcode_10"></div>
			</div>
		</div>
	</div>
</div>
			<div class="question_insertcode" id="question_insertcode_11"></div>
<div class="container index" id="showanswerform">
	<div class="row">
		<div class="col-md-17 main " style="padding: 0px;">
			<div class="question_insertcode" id="question_insertcode_12"></div>
			<div class="note ">
				<div class="post">
					<div class="comment-list">
					<!-- 提交回答 -->
{template solve_question_postanswer}
<div class="question_insertcode" id="question_insertcode_13"></div>
						<div id="comment-list" class="comment-list bb"
							style="margin: 0px; margin-bottom: 20px;">

							<div id="normal-comment-list" class="normal-comment-list">
								<div>
									<div>
										<div class="top" id="comments">
											<span>{if $bestanswer['id']==null}{$rownum}{else}{eval echo $rownum+1;}{/if}条回答</span>

											<div class="pull-right"></div>
										</div>
									</div>
									<!-- 最佳答案展示 -->
									{template solve_question_bestanswer}
									<div class="question_insertcode" id="question_insertcode_14"></div>
								<!-- 其它回答列表 -->
									{template solve_question_otheranswerlist}
									<div class="question_insertcode" id="question_insertcode_15"></div>
								</div>
							</div>
							<div></div>
						</div>
		
				<!-- 一快捷邀请回答列表-->
{template solve_question_invatelist}
	<div class="question_insertcode" id="question_insertcode_16"></div>
	<!-- 一周热门问题-->
{template solve_question_weekhotlist}
<div class="question_insertcode" id="question_insertcode_17"></div>
					</div>
				</div>


			</div>

		</div>

		<div class="col-md-7  aside ">		
		<div class="question_insertcode" id="question_insertcode_18"></div>	
					<!-- 相关问题-->
{template solve_question_qlist}
<div class="question_insertcode" id="question_insertcode_19"></div>
			<!--相关文章 -->
{template solve_question_articlelist}
<div class="question_insertcode" id="question_insertcode_20"></div>
		</div>
	</div>
</div>

<div class="modal fade" id="dialogadopt">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">采纳回答</h4>
			</div>
			<div class="modal-body">

				<form class="form-horizontal" name="editanswerForm" method="post">
					<input type="hidden" value="{$question['id']}" id="adopt_qid"
						name="qid" /> <input type="hidden" id="adopt_answer" value="0"
						name="aid" />
					<table class="table ">
						<tr valign="top">
							<td>向帮助了您的网友说句感谢的话吧!</td>
						</tr>
						<tr>
							<td>
								<div class="inputbox mt15">
									<textarea class="form-control" id="adopt_txtcontent"
										name="content">非常感谢!</textarea>
								</div>
							</td>
						</tr>
						<tr>
							<td><button type="button" id="adoptbtn" class="btn btn-success">确&nbsp;认</button></td>
						</tr>
					</table>
				</form>

			</div>

		</div>
	</div>
</div>
<script>

function checkpay(_qid,_aid){
	
		var url="{SITE_URL}answerpay/poppay/"+_qid+"/"+_aid+"{$setting['seo_suffix']}";
		var myModalTrigger = new $.zui.ModalTrigger({url:url});
		myModalTrigger.show({
'backdrop':'static'
		});

	
}
if(typeof($(".work-show-box").find("img").attr("data-original"))!="undefined"){
	var imgurl=$(".work-show-box").find("img").attr("data-original");
	$(".work-show-box").find("img").attr("src",imgurl);
}
$(".work-show-box,.answercontent").find("img").attr("data-toggle","lightbox").attr("data-lightbox-group",Date.parse( new Date() ).toString());
var postadopt=false;
$("#adoptbtn").click(function(){
	  var data={
    			content:$("#adopt_txtcontent").val(),
    			qid:$("#adopt_qid").val(),
    			aid:$("#adopt_answer").val()

    	}
  	if(postadopt){
  	  	return false;
  	}
	  postadopt=true;
	$.ajax({
	    //提交数据的类型 POST GET
	    type:"POST",
	    //提交的网址
	    url:"{url question/ajaxadopt}",
	    //提交的数据
	    data:data,
	    //返回数据的格式
	    datatype: "json",//"xml", "html", "script", "json", "jsonp", "text".
	    //在请求之前调用的函数
	    beforeSend:function(){},
	    //成功返回之后调用的函数
	    success:function(data){
	    	var data=eval("("+data+")");
	       if(data.message=='ok'){
	    	   new $.zui.Messager('采纳成功!', {
	    		   type: 'success',
	    		   close: true,
	       	    placement: 'center' // 定义显示位置
	       	}).show();
	    	   setTimeout(function(){
	               window.location.reload();
	           },1500);
	       }else{
	    	   new $.zui.Messager(data.message, {
	        	   close: true,
	        	    placement: 'center' // 定义显示位置
	        	}).show();
	       }


	    }   ,
	    //调用执行后调用的函数
	    complete: function(XMLHttpRequest, textStatus){
	     	postadopt=true;
	    },
	    //调用出错执行的函数
	    error: function(){
	        //请求出错处理
	    	postadopt=false;
	    }
	 });
})

</script>
</div>
<!-- 编辑标签 -->

<div class="modal fade" id="dialog_tag">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">编辑标签</h4>
			</div>
			<div class="modal-body">

				<form onsubmit=" return checktagsubmit()" class="form-horizontal"
					name="edittagForm" action="{url question/edittag}" method="post">
					<input type="hidden" value="{$question['id']}" name="qid" />

					<p>最多设置5个标签!</p>

					<div class="inputbox mar-t-1">
						<div class=" dongtai ">
							<div class="tags">
								{loop $taglist $tag}
								<div class="tag">
									<span tagid="{$tag['id']}">{$tag['tagname']}</span><i
										class="fa fa-close"></i>
								</div>
								{/loop}
							</div>
							<input type="text" autocomplete="off" data-toggle="tooltip"
								data-placement="bottom" title=""
								placeholder="检索标签，最多添加5个,添加标签更容易被回答"
								data-original-title="检索标签，最多添加5个" name="topic_tagset" value=""
								class="txt_taginput"> <i class="fa fa-search"></i>
							<div class="tagsearch"></div>

						</div>

						<input type="hidden" class="form-control" id="qtags" name="qtags"
							value="" />
					</div>

					<div class="mar-t-1">

						<button type="submit" class="btn btn-success">保存</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					</div>
				</form>

			</div>

		</div>
	</div>
</div>
<!--{if 1==$user['grouptype'] && $user['uid']}-->
<div class="modal fade" id="catedialog">
	<div class="modal-dialog modal-md" style="width: 460px; top: 50px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">修改分类</h4>
			</div>
			<div class="modal-body">

				<div id="dialogcate">
					<form class="form-horizontal" name="editcategoryForm"
						action="{url question/movecategory}" method="post">
						<input type="hidden" name="qid" value="{$question['id']}" /> <input
							type="hidden" name="category" id="categoryid" /> <input
							type="hidden" name="selectcid1" id="selectcid1"
							value="{$question['cid1']}" /> <input type="hidden"
							name="selectcid2" id="selectcid2" value="{$question['cid2']}" />
						<input type="hidden" name="selectcid3" id="selectcid3"
							value="{$question['cid3']}" />
						<table class="table ">
							<tr valign="top">
								<td><select id="category1" class="catselect" size="8"
									name="category1"></select></td>
								<td align="center" valign="middle"><div style="display: none;"
										id="jiantou1">>></div></td>
								<td><select id="category2" class="catselect" size="8"
									name="category2" style="display: none"></select></td>
								<td align="center" valign="middle"><div style="display: none;"
										id="jiantou2">>>&nbsp;</div></td>
								<td><select id="category3" class="catselect" size="8"
									name="category3" style="display: none"></select></td>
							</tr>
							<tr>
								<td colspan="5"><span> <input type="submit"
										class="btn btn-success" value="确&nbsp;认"
										onclick="change_category();" /></span> <span>
										<button type="button" class="btn btn-default mar-lr-1"
											data-dismiss="modal">关闭</button>
								</span></td>
							</tr>
						</table>
					</form>
				</div>

			</div>

		</div>
	</div>
</div>
<!--{/if}-->
<!-- 举报 -->
<div class="modal fade panel-report" id="dialog_inform">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">举报内容</h4>
			</div>
			<div class="modal-body">

				<form id="rp_form" class="rp_form" action="{url inform/add}"
					method="post">
					<input value="" type="hidden" name="qid" id="myqid"> <input
						value="" type="hidden" name="aid" id="myaid"> <input value=""
						type="hidden" name="qtitle" id="myqtitle">
					<div class="js-group-type group group-2">
						<h4>检举类型</h4>
						<ul>
							<li class="js-report-con"><label><input type="radio"
									name="group-type" value="1"><span>检举内容</span></label></li>
							<li class="js-report-user"><label><input type="radio"
									name="group-type" value="2"><span>检举用户</span></label></li>
						</ul>
					</div>
					<div class="group group-2">
						<h4>检举原因</h4>
						<div class="list">
							<ul>
								<li><label class="reason-btn"><input type="radio" name="type"
										value="4"><span>广告推广</span></label></li>
								<li><label class="reason-btn"><input type="radio" name="type"
										value="5"><span>恶意灌水</span></label></li>
								<li><label class="reason-btn"><input type="radio" name="type"
										value="6"><span>回答内容与提问无关</span> </label></li>
								<li><label class="copy-ans-btn"><input type="radio" name="type"
										value="7"><span>抄袭答案</span></label></li>
								<li><label class="reason-btn"><input type="radio" name="type"
										value="8"><span>其他</span></label></li>
							</ul>
						</div>
					</div>
					<div class="group group-3">
						<h4>检举说明(必填)</h4>
						<div class="textarea">
							<ul class="anslist"
								style="display: none; line-height: 20px; overflow: auto; height: 171px;">
							</ul>
							<textarea name="content" maxlength="200"
								placeholder="请输入描述200个字以内">
</textarea>
						</div>
					</div>
					<div class="mar-t-1">

						<button type="submit" id="btninform" class="btn btn-success">提交</button>
						<button type="button" class="btn btn-default mar-ly-1"
							data-dismiss="modal">关闭</button>
					</div>
				</form>


			</div>

		</div>
	</div>
</div>


<!-- 邀请回答 -->

<div class="modal fade" id="dialog_invate">
	<div class="modal-dialog" style="width: 700px; top: -30px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title"></h4>
				<div class="m_invateinfo">
					<span class="m_i_text""> 您已邀请<span class="m_i_persionnum">15</span>人回答
					</span> <span data-toggle="popover" data-tip-class="popover-info"
						data-html="ture" data-placement="bottom" data-content=""
						title="我的邀请列表" class="m_i_view">查看邀请</span>

					<div class="m_i_warrper">
						<input data-qid="{$question['id']}" type="text"
							id="m_i_searchusertxt" class="m_i_search" placeholder="搜索你想邀请的人">
						<i class="fa fa-search"></i>
					</div>

				</div>
			</div>
			<div class="modal-body">
				<!-- 邀请回答 -->
				<ul class="trigger-menu m_invate_tab"
					data-pjax-container="#list-container">
					<li class="active" data-qid="{$question['id']}" data-item="1"><a
						href="javascript:">擅长该话题的人</a></li>
					<li class="" data-qid="{$question['id']}" data-item="2"><a
						href="javascript:"> 回答过该话题的人</a></li>
					<li class="" data-qid="{$question['id']}" data-item="3"><a
						href="javascript:">我关注的人</a></li>

				</ul>
				<!-- 邀请回答列表 -->
				<div class="m_invatelist"></div>

			</div>

		</div>
	</div>
</div>

<script>
  getquestioncaozuo(1,{$question['id']});
  getquestioncaozuo(2,{$question['id']});
  getquestioncaozuo(3,{$question['id']});
  getquestioncaozuo(4,{$question['id']});
  $(".btnshowall").click(function(){
    $(".shortquestioncontent").toggle();
    $(".hidequestioncontent").toggle();
  });
  $("#normal-comment-list .answercontent").each(function(){
		if($(this).height()>150){
			$(this).parent().find(".readmore").show();
		}else{
			$(this).parent().find(".readmore").hide();
		}
	});
  <!--{if $setting['code_ask']}-->
  var needcode=1;
  <!--{else}-->
  var needcode=0;
    <!--{/if}-->
  var g_id = {$user['groupid']};
  var qid = {$question['id']};
  function listertext(){
  	 var _content=$("#anscontent").val();
  	 if(_content.length>0&&g_id!=1){

  		 $(".code_hint").show();
  	 }else{
  		 $(".code_hint").hide();
  	 }
  }
  {if $setting['mobile_localyuyin']==0}
  var mobile_localyuyin=0;
  {else}
  var mobile_localyuyin=1;
  {/if}
	//  var userAgent = window.navigator.userAgent.toLowerCase();
	//  $.browser.msie8 = $.browser.msie && /msie 8\.0/i.test(userAgent);
	 // if($.browser.msie8==true){
		//  var mobile_localyuyin=0;
	 // }
  var targetplay=null;
  function checktagsubmit(){
if(gettagsnum()<=0){
	alert("请设置标签");
	return false;
}
if(gettagsnum()>5){
    alert("最多添加5个标签");
    return false;
	}
	 var _tagstr=gettaglist();
	 $("#qtags").val(_tagstr);
	 
  }
  $(".txt_taginput").on(" input propertychange",function(){
		 var _txtval=$(this).val();
		 if(_txtval.length>1){
		
			 //检索标签信息
			 var _data={tagname:_txtval};
			 var _url="{url tags/ajaxsearch}";
			 function success(result){
				 console.log(result)
				 if(result.code==200){
					 console.log(_txtval)
					  $(".tagsearch").html("");
					for(var i=0;i<result.taglist.length;i++){
				
						 var _msg=result.taglist[i].tagname
						 
				           $(".tagsearch").append('<div class="tagitem" tagid="'+result.taglist[i].id+'">'+_msg+'</div>');
					}
					$(".tagsearch").show();
					$(".tagsearch .tagitem").click(function(){
						var _tagname=$.trim($(this).html());
						var _tagid=$.trim($(this).attr("tagid"));
						if(gettagsnum()>=5){
							alert("标签最多添加5个");
							return false;
						}
						if(checktag(_tagname)){
							alert("标签已存在");
							return false;
						}
						$(".dongtai .tags").append('<div class="tag"><span tagid="'+_tagid+'">'+_tagname+"</span><i class='fa fa-close'></i></div>");
						$(".dongtai .tags .tag  .fa-close").click(function(){
							$(this).parent().remove();
						});
						$(".tagsearch").html("");
						$(".tagsearch").hide();
						$(".txt_taginput").val("");
						});
			        
				 }
				 
			 }
			 ajaxpost(_url,_data,success);
		 }else{
				$(".tagsearch").html("");
				$(".tagsearch").hide();
		 }
	})
		function checktag(_tagname){
			var tagrepeat=false;
			$(".dongtai .tags .tag span").each(function(index,item){
				var _tagnametmp=$.trim($(this).html());
				if(_tagnametmp==_tagname){
					tagrepeat=true;
				}
			})
			return tagrepeat;
		}
		function gettaglist(){
			var taglist='';
			$(".dongtai .tags .tag span").each(function(index,item){
				var _tagnametmp=$.trim($(this).attr("tagid"));
				taglist=taglist+_tagnametmp+",";
				
			})
			taglist=taglist.substring(0,taglist.length-1);
		
			return taglist;
		}
		function gettagsnum(){
	      return $(".dongtai .tags .tag").length;
		}
		$(".tagsearch .tagitem").click(function(){
			var _tagname=$.trim($(this).html());
			if(gettagsnum()>=5){
				alert("标签最多添加5个");
				return false;
			}
			if(checktag(_tagname)){
				alert("标签已存在");
				return false;
			}
			$(".dongtai .tags").append('<div class="tag"><span>'+_tagname+"</span><i class='fa fa-close'></i></div>");
			$(".dongtai .tags .tag  .fa-close").click(function(){
				$(this).parent().remove();
			});
			$(".tagsearch").html("");
			$(".tagsearch").hide();
			$(".txt_taginput").val("");
			});
		$(".dongtai .tags .tag  .fa-close").click(function(){
			$(this).parent().remove();
		});
  $(".yuyinplay").click(function(){
  	targetplay=$(this);
  	var _serverid=targetplay.attr("id");
  	   if(_serverid == '') {
  			alert('语音文件丢失');
             return;
         }
  	   $(".wtip").html("免费偷听");
  	   targetplay.find(".wtip").html("播放中..");
  	   if(mobile_localyuyin==1){
  		 $(".htmlview").removeClass("hide");
		   $(".ieview").addClass("hide");

  		   var myAudio =targetplay.find("#voiceaudio")[0];
  	  	  // myAudio.pause();
  	  	   //myAudio.play();
  	  	   if(myAudio.paused){
  	  		   targetplay.find(".wtip").html("播放中..");
  	             myAudio.play();
  	         }else{
  	      	   targetplay.find(".wtip").html("暂停..");
  	             myAudio.pause();
  	         }
  	  	   function endfun(){ targetplay.find(".wtip").html("播放结束");alert("播放结束!")}
  	  	   var   is_playFinish = setInterval(function(){
  	             if( myAudio.ended){

  	          	   endfun();
  	  	                    window.clearInterval(is_playFinish);
  	             }
  	     }, 10);
  	   }else{

  		 $(".ieview").removeClass("hide");
  		   $(".htmlview").addClass("hide");
  	   }




  })
function deleteanswer(current_aid){
	  if(confirm("是否删除此回答?")){
			window.location.href=g_site_url + "index.php" + query + "question/deleteanswer/"+current_aid+"/$question['id']";

	  }

}
  function adoptanswer(aid) {

      $("#adopt_answer").val(aid);

      $('#dialogadopt').modal('show');
}
  //编辑标签
  function edittag() {
 	 $('#dialog_tag').modal('show');

 }
  if(typeof($(".show-content").find("img").attr("data-original"))!="undefined"){
		var imgurl=$(".show-content").find("img").attr("data-original");
		$(".show-content").find("img").attr("src",imgurl);
	}

	$(".show-content,.answercontent").find("img").attr("data-toggle","lightbox").attr("data-lightbox-group",Date.parse( new Date() ).toString());

  var category1 = {$categoryjs[category1]};
  var category2 = {$categoryjs[category2]};
  var category3 = {$categoryjs[category3]};
  var selectedcid = "{$question['cid1']},{$question['cid2']},{$question['cid2']}";
  //修改分类
  function change_category() {
      var category1 = $("#category1 option:selected").val();
              var category2 = $("#category2 option:selected").val();
              var category3 = $("#category3 option:selected").val();
              if (category1 > 0) {
      $("#categoryid").val(category1);
      }
      if (category2 > 0) {
      $("#categoryid").val(category2);
      }
      if (category3 > 0) {
      $("#categoryid").val(category3);
      }
      $("#catedialog").model("hide");
              $("form[name='editcategoryForm']").submit();
      }
  //投诉
  function openinform(qid ,qtitle,aid) {
	  $("#myqid").val(qid);
	  $("#myqtitle").val(qtitle);
	  $("#myaid").val(aid);
 	 $('#dialog_inform').modal('show');

 }
  $(".showcommentid").each(function(){
       var dataid=$(this).attr("data-id");
       show_comment(dataid);
  });
  function show_comment(answerid) {
      if ($("#comment_" + answerid).css("display") === "none") {
      load_comment(answerid);
              $("#comment_" + answerid).slideDown();
      } else {
      $("#comment_" + answerid).slideUp();
      }
      }
  //添加评论
  function addcomment(answerid) {
  var content = $("#comment_" + answerid + " input[name='content']").val();
  var replyauthor = $("#comment_" + answerid + " input[name='replyauthor']").val();
 
  if (g_uid == 0){
	  
      login();
      return false;
  }
  if (bytes($.trim(content)) < 5){
  alert("评论内容不能少于5字");
          return false;
  }
  $.ajax({
  type: "POST",
          url: "{url answer/addcomment}",
          data: "content=" + content + "&answerid=" + answerid+"&replyauthor="+replyauthor,
          success: function(status) {
          if (status == '1') {
          $("#comment_" + answerid + " input[name='content']").val("");
                  load_comment(answerid);
          }else{
          	if(status == '-2'){
          		alert("问题已经关闭，无法评论");
          	}
          }
          }
  });
  }
  
  //删除评论
  function deletecomment(commentid, answerid) {
  if (!confirm("确认删除该评论?")) {
  return false;
  }
  $.ajax({
  type: "POST",
          url: "{url answer/deletecomment}",
          data: "commentid=" + commentid + "&answerid=" + answerid,
          success: function(status) {
              
          if (status == '1') {
          load_comment(answerid);
          }else{
                alert(status);
          }
          
          }
  });
  }
  //加载评论
  function load_comment(answerid){
  $.ajax({
  type: "GET",
          cache:false,
          url: "{SITE_URL}index.php?answer/ajaxviewcomment/" + answerid,
          success: function(comments) {
          $("#comment_" + answerid + " .my-comments-list").html(comments);
          }
  });
  }

  function replycomment(commentauthorid,answerid){
      var comment_author = $("#comment_author_"+commentauthorid).attr("title");
      $("#comment_"+answerid+" .comment-input").focus();
      $("#comment_"+answerid+" .comment-input").val("回复 "+comment_author+" :");
      $("#comment_" + answerid + " input[name='replyauthor']").val(commentauthorid);
  }
	$(function(){
		  initcategory(category1);
          fillcategory(category2, $("#category1 option:selected").val(), "category2");
          fillcategory(category3, $("#category2 option:selected").val(), "category3");
      	var qrurl="{url question/view/$question['id']}";
    	//微信二维码生成
    	$('#qr_wxcode').qrcode(qrurl);
         //显示微信二维码
         $(".share-weixin").click(function(){
        	 $(".share-wechat").show();
         });
         //关闭微信二维码
         $(".close").click(function(){
        	 $(".share-wechat").hide();
        	 $(".pay-money").hide();
         });
	})
                </script>
<!--{template footer}-->