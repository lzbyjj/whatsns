<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"> 
	<meta name="referrer" content="no-referrer"/>
	<title>{$navtitle}-whatsns人工智能</title>
	 <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
     
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<meta name="format-detection" content="email=no">
	<link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">  
	<script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
	<script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<head lang="ch">
    <meta charset="UTF-8">


</head>
<body>
<div class="container">
  <div class="row">
    <div class="col-md-12">
    <form role="form" action="{url ai/getarticle}" method="post">
	<div class="form-group" style="margin-top: 20px;">
		<label for="name">复制文章详情页面网址：</label>
		<input type="text" class="form-control" id="url" name="url" value="{$url}"			   placeholder="粘贴文章详情页面网址">
	</div>
	<button type="submit" name="posturl" class="btn btn-default">抓取</button>
</form>
  </div>
  </div>
  <div>
  {$errormsg}
  </div>
    <div class="row">
    <div class="col-md-8">
    <div class="copyarticle">
       <h1>
    {$title}
    </h1>
    <article>
    {if !empty($imagethumb)&&empty($playurl)}
    <img src="$imagethumb" style="max-width:100%;margin-bottom:5px;"/>
    {/if}
       {if !empty($imagethumb)&&!empty($playurl)}
       <video controls poster="$imagethumb" style="max-width:100%;margin-bottom:5px;">
   <source src="$playurl" type="video/mp4">
  你的浏览器不支持播放视频
</video> 
    {/if}
     {$content}
    </article>
    </div>
 
  </div>

  </div>
  
</div>

	
</body>
</html>