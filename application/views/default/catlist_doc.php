<!DOCTYPE HTML>
<html lang="zh-hans" >
  <!--{eval global $starttime,$querynum;$mtime = explode(' ', microtime());$runtime=number_format($mtime[1] + $mtime[0] - $starttime,6); $setting=$this->setting;$user=$this->user;$regular=$this->regular;$toolbars="'".str_replace(",", "','", $setting['editor_toolbars'])."'";}-->

<head>
    <meta charset="UTF-8">
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
        <!--{if isset($seo_title)}-->
        <title>{$seo_title}</title>
        <!--{else}-->
        <title><!--{if $navtitle}-->{$navtitle} - <!--{/if}-->{$setting['site_name']}</title>
        <!--{/if}-->
        <!--{if isset($seo_description)}-->
        <meta name="description" content="{$seo_description}" />
        <!--{else}-->
        <meta name="description" content="{$setting['site_name']}" />
        <!--{/if}-->

        <meta name="keywords" content="{$seo_keywords}" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="keywords" content="{$doc['keyword']}">
    <meta name="description" content="{$doc['shortinfo']}">
    <link rel="stylesheet" href="{SITE_URL}static/doc/css/style.css">
    <link rel="stylesheet" href="{SITE_URL}static/doc/css/splitter.css">
    <link rel="stylesheet" href="{SITE_URL}static/doc/css/expandable-chapters.css">
    <link rel="stylesheet" href="{SITE_URL}static/doc/css/atoc.css">
    <link rel="stylesheet" href="{SITE_URL}static/doc/css/search.css">
    <link rel="stylesheet" href="{SITE_URL}static/doc/css/website1.css">
    <link rel="stylesheet" href="{SITE_URL}static/doc/css/website.css">
    <link rel="stylesheet" href="{SITE_URL}static/doc/css/website2.css">
    <meta name="HandheldFriendly" content="true"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
</head>
<body>
{eval $newcid=0;$catmodel=array();}
            {if $catid}
            {eval $newcid=$catid;}
            {/if}
               {if $topicone}
            {eval $newcid=$topicone['articleclassid'];}
            {/if}
            {eval $cat=$this->category_model->get($newcid);}
            {if $cat['pid']==0}
            {eval $catmodel=$cat;}
            
            {else}
            {eval $parentid=$cat['pid'];}
             {eval $cat=$this->category_model->get($parentid);}
                  {if $cat['pid']==0}
            {eval $catmodel=$cat;$newcid=$cat['id'];}
            
            {else}
             {eval $parentid=$cat['pid'];}
             {eval $cat=$this->category_model->get($parentid);}
              {eval $catmodel=$cat;$newcid=$cat['id'];}
              {/if}
            {/if}
<div class="book">
    <div class="book-summary">
        <div id="book-search-input" role="search">
            <a class="backhome" href="{SITE_URL}"><i class="fa fa-home"></i>返回首页</a>
        </div>
        <nav role="navigation">
            <ul class="summary">
                <li class="chapter" >

                    <a href="">
                        简介
                    </a>
                </li>    
                      <li class="chapter" >

                    <a href="https://codeigniter.org.cn/userguide3/" target="_blank">
                                                    框架在线手册
                    </a>
                </li>        
               

		{eval $subcatlist=$this->category_model->list_by_pid ( $newcid );}
		   {if $subcatlist}
	{loop $subcatlist $subcat}
		<li class="divider"></li>
                <li class="chapter " data-level="3.1" >

                <span>


                   {$subcat['name']}

                </span>



                    <ul class="articles">
          
         		<!-- 读取当前分类以及子分类下的文章 -->
							{eval $cidlist=array();array_push($cidlist,$subcat['id']);}
							{eval $tmpsubcatlist=$this->category_model->list_by_pid ( $subcat['id'] );}
							{if  $tmpsubcatlist}
							{loop $tmpsubcatlist $tmpsubcat}
							{eval array_push($cidlist,$tmpsubcat['id']);}
							{/loop}
							{/if}
							{eval $explodecid = implode ( ',', $cidlist );}
						    {eval $subtopiclist=$this->getlistbysql("select title,id,describtion from ".$this->db->dbprefix."topic where articleclassid in($explodecid) order by displayorder desc limit 0,10");}
							{loop $subtopiclist $subt}
							 {if !$topicone}
							 {eval $topicone=$subt;$topicone['describtion'] = checkwordsglobal (htmlspecialchars_decode($topicone['describtion']) )}
							 {/if}
							
							<li class="chapter {if $id==$subt['id']}active{/if} "><a href="{url topic/getone/$subt['id']}">{$subt['title']}</a></li>
						     {/loop}
						                    
                    </ul>

                </li>
                
	{/loop}
		{/if}
        
                <li class="divider"></li>

                <li>
                 <a class="">whatsns模板开发在线文档</a>
                </li>
            </ul>
        </nav>
    </div>

    <div class="book-body">

        <div class="body-inner">
            <div class="book-header" role="navigation">
                <!-- Title -->
                <h1>
                    <i class="fa fa-circle-o-notch fa-spin"></i>
                    <a href="{url topic/getone/$topicone['id']}" >{$topicone['title']}</a>
                </h1>
            </div>
            <div class="page-wrapper" tabindex="-1" role="main">
                <div class="page-inner">

                    <div id="book-search-results">
                        <div class="search-noresults">

                            <section class="normal markdown-section">
                               
                                <h1 id="{$topicone['id']}">{$topicone['title']}</h1>
                               <article>
                               {eval echo replacewords( $topicone['describtion']);}
                               </article>

                            </section>

                        </div>
                     
                    </div>

                </div>
            </div>

        </div>
    </div>

    <script>
        var gitbook = gitbook || [];
        gitbook.push(function() {
            gitbook.page.hasChanged({"page":{"title":"{$topicone['title']}","level":"3.1.1","depth":2,"next":{"title":"{$topicone['title']}","level":"3.1.2","depth":2,"path":"{url topic/getone/$topicone['id']}","ref":"tech/use-mip-to-create-mobile-web-pages.md","articles":[]},"previous":{"title":"{$catmodel['name']}","level":"3.1","depth":1,"ref":"","articles":[{"title":"{$topicone['title']}","level":"3.1.1","depth":2,"path":"tech/fis.md","ref":"tech/fis.md","articles":[]},{"title":"{$doc['title']}","level":"3.1.2","depth":2,"path":"tech/use-mip-to-create-mobile-web-pages.md","ref":"tech/use-mip-to-create-mobile-web-pages.md","articles":[]},{"title":"{$doc['title']}","level":"3.1.3","depth":2,"path":"tech/lavas-pwa.md","ref":"tech/lavas-pwa.md","articles":[]}]},"dir":"ltr"},"config":{"plugins":["splitter","expandable-chapters","atoc","-sharing","-lunr","-search","search-pro","baidu","livereload"],"styles":{"website":"styles/website.css","pdf":"styles/pdf.css","epub":"styles/epub.css","mobi":"styles/mobi.css","ebook":"styles/ebook.css","print":"styles/print.css"},"pluginsConfig":{"livereload":{},"atoc":{},"splitter":{},"search-pro":{},"fontsettings":{"theme":"white","family":"sans","size":2},"highlight":{},"baidu":{"token":"c7d52dcc025ace5582c34ce31e4b4552"},"theme-default":{"styles":{"website":"styles/website.css","pdf":"styles/pdf.css","epub":"styles/epub.css","mobi":"styles/mobi.css","ebook":"styles/ebook.css","print":"styles/print.css"},"showLevel":false},"expandable-chapters":{}},"theme":"default","pdf":{"pageNumbers":true,"fontSize":12,"fontFamily":"Arial","paperSize":"a4","chapterMark":"pagebreak","pageBreaksBefore":"/","margin":{"right":62,"left":62,"top":56,"bottom":56}},"structure":{"langs":"LANGS.md","readme":"README.md","glossary":"GLOSSARY.md","summary":"SUMMARY.md"},"variables":{},"title":"{$doc['title']}","language":"zh-hans","gitbook":"*","description":"{$doc['shortinfo']}"},"file":{"path":"tech/fis.md","mtime":"2017-07-26T09:04:06.000Z","type":"markdown"},"gitbook":{"version":"3.2.2","time":"2017-11-21T08:02:24.159Z"},"basePath":"..","book":{"language":""}});
        });
    </script>
</div>
<script src="{SITE_URL}static/doc/css/gitbook.js"></script>
<script src="{SITE_URL}static/doc/css/theme.js"></script>
<script src="{SITE_URL}static/doc/css/expandable-chapters.js"></script>
<script src="{SITE_URL}static/doc/css/fontsettings.js"></script>
		<script src="{SITE_URL}static/js/jquery-1.11.3.min.js"></script>

   <!--{if $setting['cancopy']==1}-->
              <script src="{SITE_URL}static/js/nocopy.js"></script>
                <!--{/if}-->

<script src="{SITE_URL}static/js/jquery.lazyload.min.js"></script>
<script>

 <!--{if $setting['opensinglewindow']==1}-->
 $("a").attr("target","_self");

                <!--{/if}-->
  

                    $("img.lazy").lazyload({effect: "fadeIn" });

</script>
<div class="hide">     <!--{if $setting['site_statcode']}--> {eval echo decode($setting['site_statcode'],'tongji');}<!--{/if}-->
                    
                     </div>
</body>
</html>