{if !is_mobile()}
<style>
	.mb20 {
    margin-bottom: 20px;
}
#article_index {
    border: 0;
}
.fl {
    float: left;
}
#article_index a:link, #article_index a:visited {
    text-decoration: none;
}
#article_index .img-responsive {
    display: block;
    max-width: 100%;
    width: 240px;
    height: 147px;
}

#article_index img {
    border: none;
}
#article_index .layui-btn, #article_index .layui-edge, #article_index .layui-inline, img {
    vertical-align: middle;
}
#article_index a {
    text-decoration: none;
    color: initial;
    display: block;
}
#article_index .aw-item {
    overflow: hidden;
    cursor: pointer;
    background-color: #fff;
    padding-bottom: 20px;
    padding-top:20px;
    border-bottom: 1px solid #f0f0f0;
}
#article_index .aw-item:last-child {
    border-bottom: 1px solid #f0f0f0;
}
#article_index .aw-item:first-child {
    border-top: 1px solid #f0f0f0;
}
#article_index .item-right {
    width: 420px;
    position: relative;
    height: 160px;
}
#article_index .fr {
    float: right;
}
#article_index .tit_20 {
    font-size: 20px;
    overflow: hidden;
    width: 100%;
    margin-top: -4px;
}
#article_index .pb20 {
    padding-bottom: 20px;
}
#article_index h1,#article_index  h2,#article_index h3,#article_index h4 {
    font-weight: 400;
}
#article_index .item-right p {
    font-size: 15px;
}
#article_index .color6 {
    color: #666;
}
#article_index .awfo-box {
    height: 22px;
    line-height: 22px;
    color: #666;
    position: absolute;
    bottom: 8px;
}
</style>
<div class="index-item mod-body mb20" id="article_index">
	 {loop $tagdoinglist $tagdoing}
   <div class="aw-item" {if !$tagdoing['image']}style="height:130px;"{/if}>
   	{if $tagdoing['image']}
			    <div class="fl">
			    							<a href="{$tagdoing['url']}" data-fancybox-group="thumb" rel="lightbox">
							<img width="240" height="160" class="img-responsive" src="{$tagdoing['image']}">
						</a> 
						 
			    </div>
			    {/if}
			    <div class="item-right 	{if $tagdoing['image']}fr{/if}" {if !$tagdoing['image']}style="width:100%;height:130px;"{/if}>
			        <h4 class="tit_20 pb20">
			            <a href="{$tagdoing['url']}" title="$tagdoing['title']">
			            	{if !$tagdoing['image']}
			            	{eval echo clearhtml($tagdoing['title'],40);}
			            
			            	{else}
			            		{eval echo clearhtml($tagdoing['title'],20);}
			            	{/if}
			            		</a>
			        </h4>	 
			        <p class="color6 f15tent">	{eval echo clearhtml($tagdoing['miaosu'],80);}</p>
			        <div class="awfo-box f13">
			            <span class="aw-name ">{$tagdoing['author']}</span>
			            <span class="aw-name ">.</span>
			             <span class="aw-name ">{$tagdoing['typename']}{$tagdoing['nums']}</span>
			           
			                			        </div>			    
			    </div>
			</div>
			{/loop}
        </div>
        {else}
          <style>
          	.mb10 {
    margin-bottom: 0.1rem;
}

.boxys {
    width: 100%;
    /* font-size: .3rem; */
    height: auto;
    border-bottom: 1px solid #f5f5f5;
    padding: .36rem .44rem .46rem;
    overflow: hidden;
}
.plist li {
    padding: 10px;
    border-bottom: 1px solid #f0f0f0;
}

.boxys {
    color: #999;
}
.boxys .tent {
    position: relative;
    width: 100%;
    overflow: hidden;
    height: auto;
}
.boxys .tent a {
    display: flex;
}
.boxys .tent .tent-tent {
    position: relative;
    /* height: 100px; */
    width: 60%;
    flex: 1;
}
.fl {
    float: left;
}
.boxys .tent h2 {
    font-size: 16px;
    line-height: 28px;
    margin: 0;
    color:#333;
    margin-bottom: 10px;
    display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 2;
    overflow: hidden;
}
.box-bottom {
    /* position: absolute; */
    /* bottom: -3px; */
    clear: both;
    font-size: 12px;
    color: #999;
    margin: 0;
    height: 20px;
}

.comment-span, .look-span {
    padding: 0 5px !important;
    height: 20px;
    line-height: 20px;
    position: relative;
}
.comment-span {
    height: 20px;
    line-height: 20px;
    position: relative;
}
.name-span {
    padding-right: 20px;
    margin-top: 2px;
    margin-right: 10px;
}
.comment-span>img, .look-span>img {
    position: absolute;
    top: 3px;
    right: 20px;
    left: -10px;
}
.box-bottom img {
    width: 14px;
    height: 14px;
}
.boxys .tent .tent-img img {
    width: 140px;
    /* height: 100px; */
    overflow: hidden;
    float: right;
}
.boxys img {
    vertical-align: middle;
}
          </style>
        <ul class="plist mb10" id="load_explore_list">
        	 {loop $tagdoinglist $tagdoing}
        	<li class="boxys">
        <div class="tent">
													<a href="{$tagdoing['url']}">
				<div class="fl tent-tent">
					<h2> 
					    
			            	{if !$tagdoing['image']}
			            	{eval echo clearhtml($tagdoing['title'],40);}
			            
			            	{else}
			            		{eval echo clearhtml($tagdoing['title'],20);}
			            	{/if}
			            	
					
					</h2>
				 
                    <p class="box-bottom">
                        <span class="name-span fl">{$tagdoing['author']}</span>
												<span class="comment-span fl plr10"><img src="//m.shangc.net/static/wap/images/comment-gray.png"><b>{$tagdoing['nums']}</b></span>
						                    </p>
                </div>
                	{if $tagdoing['image']}
				                <div class="tent-img fr">
									
					 	 	 	<img src="https://i.shangc.net/article/20200810/170x110_48a675d4cf6e7deb0508c0d685673bb3.jpg" alt="11_(1).jpg">					 	 	 						 	 	 													</div>
					 	 	 	{/if}
            </a>
        </div>
    </li>
    	{/loop}
        	</ul>
        	{/if}