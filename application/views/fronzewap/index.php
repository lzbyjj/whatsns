<!--{template header}-->
     
      
        <div class="wl-bannerbox">
                <div class="wl-banner">
                        <a href="{url new/default/2}"><img src="{SITE_URL}static/css/fronze/index/img/banner.png" width="100%" height="auto"></a>
                    </div>

                    <div class="imgmenu">
                            <div class="imgmenubox">
                                <a href="{url ask/index}">
                                    <img src="{SITE_URL}static/css/fronze/index/img/wt.png" width="46" height="46">
                                    <span>问题广场</span>
                                </a>
                            </div>
                            <div class="imgmenubox">
                                    <a href="{url seo/index}">
                                        <img src="{SITE_URL}static/css/fronze/index/img/wz.png" width="46" height="46">
                                        <span>文章资讯</span>
                                    </a>
                            </div>
                            <div class="imgmenubox">
                                    <a href="{url expert/default}">
                                        <img src="{SITE_URL}static/css/fronze/index/img/zj.png" width="46" height="46">
                                        <span>行业专家</span>
                                    </a>
                            </div>
                         <div class="imgmenubox">
                                    <a href="{url tags}">
                                        <img src="{SITE_URL}static/css/fronze/index/img/kc.png" width="46" height="46">
                                        <span>标签库</span>
                                    </a>
                            </div>
                        </div>
        </div>

        <div class="pub-container">
            <div class="inner-box">
                <div class="quick-pub-container">
                        <div class="count-container" >
                            <h3 class="count-title">{$setting['site_name']}</h3>
                            <div class="count-detail">
                                <p class="count-desc">已帮助 <span>{eval echo  returnarraynum ( $this->db->query ( getwheresql ( 'question'," status in(2,6,9) and answers>0 ", $this->db->dbprefix ) )->row_array () ) ;}</span> 位网友解决了问题  </p>
                            </div>
                            <div class="button-box">
                                <a href="{url question/add}" class="button-item">我要立即发布问题</a>
                            </div>
                            <div class="button-box ">
                                    <a href="{url ask/index/all/xuanshang}" class="button-item nobk">去回答</a>
                            </div>
                        </div>
                </div>
            </div>
        </div>


        <div class="kecheng-container">
                <div class="title" >
                        <p class="title-text" >热门话题</p>
                        <a href="{url category/viewtopic/question}" class="title-more" >更多></a>
                    </div>
                    <div class="description" >
                        <span class="desc-item" >热点动态</span>
                        <span class="desc-item" >全民讨论</span>
                        <span class="desc-item" >精准回答</span>
                    </div>

                    <div class="content">
                            <div class="swiper-container">
                                    <ul class="swiper-wrapper">
                                                          {eval $indexcatnum= isset($setting['list_indextopiccat'])&&$setting['list_indextopiccat']>0 ? intval($setting['list_indextopiccat']):6;}
      {eval  $huatilist=$this->getlistbysql("select id,questions,name,displayorder,grade from ".$this->db->dbprefix."category where iscourse=0 and isuseask=1 and grade=1 and pid=0 order by displayorder asc  limit 0,$indexcatnum");}
        {if $huatilist}
        
           <!--{loop $huatilist $index $huati}-->
                  {if $index<$indexcatnum}
                  
                                        <li class="swiper-huati">
                                            <a href="{eval echo getcaturl($huati['id'],'category/view/#id#');}"><img width="100" height="100" src="{eval echo get_cid_dir($huati['id']);}"></a>
                                            <div class="text-box">
                                                <div class="text-name hide">{$huati['name']}</div>
                                                <div class="text-number">共{$huati['questions']}个问题</div>
                                            </div>
                                        </li>
                                                        
           {/if}
            <!--{/loop}-->
         {/if}
         
                                    </ul>
                                </div>
                    </div>
        </div>
          <!--{eval $expertlist=$this->fromcache('expertlist');}-->
         
        <section class="hot-expert expert-list white-box" id="quickout">
<div class="newlit">
                <div class="title">
                        <p class="title-text">热门专家</p>
                        <a href="{url expert/default}" class="title-more">更多&gt;</a>
                    </div>
                    </div>
<ul class="load-ul">
  	
	      <!--{loop $expertlist $expert}-->
                         <li class="clearfix"><a href="{url user/space/$expert['uid']}">
        
    {if $expert['mypay']}
    
     <span class="go-to-ask payask">咨询￥{$expert['mypay']}元</span>
          {else}
               <span class="go-to-ask">咨询</span>
                  {/if} 
            
<figure class="member head-v">
<img src="{$expert['avatar']}" alt="">
</figure> <header>{$expert['username']}</header>
<section>
<p class="content">  {$expert['signature']}</p>
<p class="ever">采纳率{eval echo $this->user_model->adoptpercent ( $expert );}%，回答{$expert['answers']}个，获得{$expert['supports']}赞</p>
</section>
</a></li>
           <!--{/loop}-->
      </ul>

</section>
  
        <div class="newlit">
                <div class="title" >
                        <p class="title-text" >最新</p>
                        <a href="{url ask/index}" class="title-more" >更多></a>
                    </div>
                <ul>
                                                         {eval  $indexquestiontilist=$this->getlistbysql("select id,description,status,title,shangjin,answers,cid from ".$this->db->dbprefix."question where status!=0  order by time desc  limit 0,10");}
        {if $indexquestiontilist}
        
           <!--{loop $indexquestiontilist $index $question}-->
                    <li>
                        <a href="{url question/view/$question['id']}">  {if $question['shangjin']>0} <span>￥{$question['shangjin']}</span>{/if}{$question['title']}</a>
                        <p>{eval echo clearhtml(htmlspecialchars_decode($question['description']),50);}</p>
                        <div class="btbox">
                            <div class="flbox">
                                    {eval $qc= $this->category[$question['cid']];}
                               {if $qc}
                                <a href="{eval echo getcaturl($qc['id'],'category/view/#id#');}">{$qc['name']}</a>
                                {/if}
                            </div>
                            <div class="frbox"><span>{$question['answers']}人</span>已参与回答</div>
                        </div>
                    </li>
                             <!--{/loop}-->
         {/if}
                </ul>
        </div>
    
	<div class="newlit">
    <div class="title" >
        <p class="title-text" >最新资讯</p>
        <a href="{url seo/index}" class="title-more" >更多></a>
    </div>
        <div class="whatsns_list" style="background:#F6F6F6;">    
            {eval  $topiclist =$this->getlistbysql("select id,describtion,articles,likes,image,state,title from ".$this->db->dbprefix."topic where state!=0  order by viewtime desc  limit 0,4");}        
            <!--{loop $topiclist $index $topic}-->   
            <div class="whatsns_listitem">
                <div class="l_title" {if !$topic['image']}style="width:100%;"{/if}>
                    <h2>
                        <a href="{url topic/getone/$topic['id']}">
                            {$topic['title']}
                        </a>
                    </h2>
                </div>
                <div class="whatsns_content">
                    {if $topic['image']}
                    <div class="weui-flex">
                        <div class="weui-flex__item">
                            <div class="imgthumbbig">
                                <a href="{url topic/getone/$topic['id']}">
                                    <img class="lazy" src="{SITE_URL}static/images/lazy.jpg" data-original="$topic['image']">
                                </a>
                            </div>
                        </div>
                    </div> 
                    {/if}
                     {if $topic['describtion']}
                    <div class="whatsns_des">
                        <div class="whatsns_readmore" onclick="window.location='{url topic/getone/$topic['id']}'">查看更多
                            <i class="fa fa-angle-down"></i>
                        </div>
                    </div>
                    {/if}
                </div>
                <div class="ask-bottom">
                    <a href="{url topic/getone/$topic['id']}" class="" ><i class="fa fa-commentingicon"></i>{$topic['articles']} 个评论</a>
                    <a href="{url topic/getone/$topic['id']}"  class=" "><i class="fa fa-qshoucang"></i>{$topic['likes']}个收藏</a>
                </div>     
            </div>     
            <!--{/loop}-->
        </div>
        </div>
  <!--{template footer}-->
