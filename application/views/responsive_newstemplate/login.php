{template meta}

<div class="layui-fulid news-login">
  <a href="index.html" class="title"><img src="{$setting['site_logo']}" alt="{$setting['site_name']}"></a>
  <div class="layui-form">
    <div class="layui-input-block">
      <input type="text" class="layui-input" autocomplete="off" id="xm-login-user-name" lay-verify="required" placeholder="用户名/手机号/邮箱">
    </div>
    <div class="layui-input-block">
      <input type="text" class="layui-input" id="xm-login-user-password" lay-verify="required" placeholder="登录密码">
    </div>
    <input type="hidden"  id="forward" name="return_url" value="{$forward}">
<input type="hidden"  id="authtype" name="authtype" value="{$_SESSION['authinfo']['type']}">
  <input type="hidden" id="tokenkey" name="tokenkey" value='{$_SESSION["logintokenid"]}'/>
    <button class="layui-btn layui-btn-lg layui-btn-normal layui-btn-fluid"  id="login_submit">登录</button>
    <div class="thirdParty"><a href="{SITE_URL}plugin/qqlogin/index.php"><i class="layui-icon layui-icon-login-qq"></i></a><a href="{url plugin_weixin/openauth}"><i class="layui-icon layui-icon-login-wechat"></i></a></div>
  </div>
</div>
{template footer}