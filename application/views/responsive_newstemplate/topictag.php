{template header}

<div class="news-search">
  <div class="layui-container">
    <div class="layui-row layui-col-space30">
      <div class="layui-col-sm8 layui-col-lg9 searchList">
        <div class="search-list">
          <ul>
          
         <!--{if $topiclist}-->
        
                 <!--{loop $topiclist $nindex $topic}-->
            <li><a href="{url topic/getone/$topic['id']}">
              <p class="title">{$topic['title']}</p>
              <p class="content">
                 {if $topic['price']!=0}
                      

  {eval echo clearhtml($topic['freeconent']);}


                   {else}
                     {eval echo clearhtml($topic['describtion']);}
                    {/if}
  
              </p>
              <p><span class="author">{$topic['author']}</span><span class="pushTime">{$topic['viewtime']}</span></p>
            </a></li>
                <!--{/loop}-->
       
          <div style="text-align: center"> <div class="laypage-main">{$departstr}</div>	</div>
             <!--{else}-->
       <div id="no-result">
                <p>抱歉，未找到和您搜索相关的内容。</p>
                <strong>建议您：</strong>
                <ul class="">
                    <li><p><span>检查输入是否正确</span></p></li>
                    <li><p><span>简化查询词或尝试其他相关词</span></p></li>
                </ul>
            </div>
    <!--{/if}-->
       </ul>
        </div>
      </div>
      <div class="layui-col-sm4 layui-col-lg3 userList index-side">
        <dl class="search-user">
          <dt>推荐用户</dt>
               <!--{eval $userarticle=$this->fromcache('userauthorlist');}-->
                  <!--{loop $userarticle $index $uarticle}-->
          <dd class="first-child">
            <a href="{url user/space/$uarticle['uid']}" class="img"><img src="{$uarticle['avatar']}"></a>
            <p class="title"><span class="">{$uarticle['username']}</span></p>
            <p><span>{$uarticle['num']}</span>篇文章</p>
            {if  $uarticle['hasfollower']}
           
           <a id="attenttouser_{$uarticle['uid']}" data-uid="{$uarticle['uid']}" class="btnattention layui-btn layui-btn-news focusOn following">已关注</a>
         
          {else}
            <a id="attenttouser_{$uarticle['uid']}" data-uid="{$uarticle['uid']}" class="btnattention layui-btn layui-btn-news focusOn follow">关注</a>
           
  {/if}
          </dd>
     <!--{/loop}-->
        </dl>
         
      </div>
    </div>
  </div>
</div>
{template footer}