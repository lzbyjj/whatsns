<div class="news-footer">
  <div class="layui-container">
    <span class="layui-breadcrumb" lay-separator=" ">
      <a>关于我们</a>
      <a>合作伙伴</a>
      <a>广告服务</a>
      <a>常见问题</a>
    </span>
    <p class="copyright">Copyright &copy; {$setting['site_name']} {$setting['site_icp']}</p>
    <a id="wechat" href=""><i class="layui-icon layui-icon-news-wechat"></i></a>
    <a id="weibo" href=""><i class="layui-icon layui-icon-news-weibo"></i></a>
    <!-- 二维码 -->
    <img id="code" src="https://wenda.whatsns.com/data/attach/logo/wxlogo.jpg">
  </div>
</div>

<script src="{SITE_URL}static/js/jquery-1.11.3.min.js"></script>
<script src="{SITE_URL}static/js/jquery.lazyload.min.js"></script>
<script>

 <!--{if $setting['opensinglewindow']==1}-->
 $("a").attr("target","_self");

                <!--{/if}-->
  

                    $("img.lazy").lazyload({effect: "fadeIn" });

</script>

<!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
<!--[if lt IE 9]>
  <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
  <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script>
  layui.config({
    base: '{SITE_URL}static/newstemplate/res/static/js/' 
  }).use('news'); 
</script>

</body>
</html>