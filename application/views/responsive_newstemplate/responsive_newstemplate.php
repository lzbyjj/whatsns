<?php
$tphtml = array (
		'meta.php' => '头部公共js和css引入文件',
		'header.php' => '公共头部文件',
		'footer.php' => '公共底部文件',
		'index.php' => '首页模板文件',
		'index_linbo.php' => '首页轮播模板文件',
		'index_rightadv1.php' => '首页右侧广告图片1模板文件',
		'index_rightadv2.php' => '首页右侧广告图片2模板文件',
		'page.php' => '分页模板文件',
		
		'seoarticlelist.php' => '文章专栏列表模板文件',
		'tip.php' => '网站跳转提示模板文件',
		'space.php' => '个人主页模板文件',
		'topicone.php' => '文章详情模板文件',
		'topictag.php' => '文章搜索列表模板文件',
);

?>