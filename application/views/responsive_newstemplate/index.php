{template header}

<div class="layui-container news-index">
	<div class="layui-row layui-col-space20 contentTop">
    <div class="layui-col-sm8">
  {template index_lunbo}
    </div>
    <div class="layui-col-xs6 layui-col-sm4">
     	  {template index_rightadv1}
    </div>
    <div class="layui-col-xs6 layui-col-sm4">
     	
     	 {template index_rightadv2}
    </div>
	 </div>
	<div class="layui-row layui-col-space20 contentBot">
    <div class="layui-col-md8 index-list">
    	<h2>推荐新闻</h2>
    	<ul class="newsList">
    	{eval $this->load->model ( 'seo_model' );}
    	{eval $pagesize = $this->setting ['list_default'];}
		
		{eval $alltopiclist = $this->seo_model->get_bycatid ( 'all', 'new', 0, $pagesize );}
		{eval $topiclist = $alltopiclist ['list'];}
		{eval $rownum = $alltopiclist ['allnum'];}
		{eval $departstr = page ( $rownum, $pagesize, $page, "seo/index/all/new" );}
		{eval $departstr = str_replace ( '.html', '', $departstr );}

    		
    		{loop  $topiclist $topic}
    		<li>
    			<a class="img" href="{url topic/getone/$topic['id']}"><img src="{$topic['image']}"></a><div class="txt">
    				<a href="{url topic/getone/$topic['id']}">{$topic['title']}</a>
    				<div class="label"><span class="orange">{$topic['category_name']}</span>{$topic['author']} <b>{$topic['viewtime']}</b> <i class="layui-icon layui-icon-news-review1"></i>{$topic['articles']}</div>
    				 {if $topic['price']!=0}
    				<p class="cont">
    				 {eval echo clearhtml($topic['freeconent'],150);}</p>
    			 {else}
                   <p class="cont">  {eval echo clearhtml($topic['describtion'],150);}</p>
                    {/if}
    			</div>
    		</li>
    		{/loop}
     
    	</ul>
     {template page}
    </div>
    <div class="layui-col-md4 index-side">
    	<div class="newsSide">
	     	<h2>热文新闻</h2>
	     	<ul>
	     	 <!--{eval $weektopiclist = $this->fromcache("weektopiclist");}-->

        <!--{loop $weektopiclist $nindex $topic}-->
        
                
	     		<li><a href="{url topic/getone/$topic['id']}">
	     		<span>
	     		<img src="{$topic['image']}">
	     		</span><p class="txt">{$topic['title']}</p></a></li>
	      <!--{/loop}-->
	     	</ul>
	    </div>
    </div>
	</div>
  <div id="silde"><a id="refresh"><i class="layui-icon layui-icon-refresh"></i></a><a id="scroll"><i class="layui-icon layui-icon-up"></i></a></div>
</div>

{template footer}