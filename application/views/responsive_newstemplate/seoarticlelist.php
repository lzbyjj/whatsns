{template header}

<div class="layui-container news-list news-index">
  <div class="layui-row layui-col-space20 contentBot">
    <div class="layui-col-md8 index-list">
      <div class="layui-tab layui-tab-brief">
        <ul class="layui-tab-title">
          <li class="{if $paixu=='new'}layui-this{/if}"><a href="{eval echo getcaturl($catid,'seo/index/#id#/new');}">最新</a></li>
          <li class="{if $paixu=='weeklist'}layui-this{/if}"><a href="{eval echo getcaturl($catid,'seo/index/#id#/weeklist');}">热门文章</a></li>
          <li class="{if $paixu=='hotlist'}layui-this{/if}"><a href="{eval echo getcaturl($catid,'seo/index/#id#/hotlist');}">推荐文章</a></li>
         <li class="{if $paixu=='credit'}layui-this{/if}"><a href="{eval echo getcaturl($catid,'seo/index/#id#/credit');}">财富阅读</a></li>
        
        </ul>
        <div class="layui-tab-content">
          <div class="layui-tab-item layui-show">
            <ul class="newsList">
             {loop  $topiclist $topic}
    		<li>
    			<a class="img" href="{url topic/getone/$topic['id']}"><img src="{$topic['image']}"></a><div class="txt">
    				<a href="{url topic/getone/$topic['id']}">{$topic['title']}</a>
    				<div class="label"><span class="orange">{$topic['category_name']}</span>{$topic['author']} <b>{$topic['viewtime']}</b> <i class="layui-icon layui-icon-news-review1"></i>{$topic['articles']}</div>
    				 {if $topic['price']!=0}
    				<p class="cont">
    				 {eval echo clearhtml($topic['freeconent'],150);}</p>
    			 {else}
                   <p class="cont">  {eval echo clearhtml($topic['describtion'],150);}</p>
                    {/if}
    			</div>
    		</li>
    		{/loop}
     
    	</ul>
       <div style="text-align: center"> <div class="laypage-main">$departstr</div>	</div>
          </div>
        </div>
      </div>
    </div>
    <div class="layui-col-md4 index-side">
   	<div class="newsSide">
	     	<h2>热文新闻</h2>
	     	<ul>
	     	 <!--{eval $weektopiclist = $this->fromcache("weektopiclist");}-->

        <!--{loop $weektopiclist $nindex $topic}-->
        
                
	     		<li><a href="{url topic/getone/$topic['id']}">
	     		<span>
	     		<img src="{$topic['image']}">
	     		</span><p class="txt">{$topic['title']}</p></a></li>
	      <!--{/loop}-->
	     	</ul>
	    </div>
    </div>
    <div id="silde"><a id="refresh"><i class="layui-icon layui-icon-refresh"></i></a><a id="scroll"><i class="layui-icon layui-icon-up"></i></a></div>
  </div>  
</div>

{template footer}