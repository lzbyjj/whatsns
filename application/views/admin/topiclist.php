<!--{template header}-->
<script type="text/javascript" src="{SITE_URL}static/js/jquery-ui/jquery-ui.js"></script>
<script src="{SITE_URL}static/js/admin.js" type="text/javascript"></script>

<style>
em{
	color:red;
}
</style>
<div style="width:100%;">
    <div >
    <ol class="breadcrumb" style="    margin-bottom: 0px;">
  <li><a href="{url admin_main/stat}">后台首页</a></li>
  <li class="active">文章管理</li>
</ol>

</div>

</div>
<div style="padding: 8px 15px;">

<!--{if isset($message)}-->
<table class="table">
    <tr>
        <td class="{if isset($type)}$type{/if}">{$message}</td>
    </tr>
</table>
<!--{/if}-->
   <a class="btn write-btn btn-success" target="_blank" href="{url admin_chajian/addarticle}">
            <i class="fa fa-pencil"></i>写文章
        </a>
           <a class="btn write-btn btn-success"  href="{url admin_topic/shenhe}">
            <i class="fa fa-check"></i>文章审核
        </a>
            <a class="btn write-btn btn-success"  href="{url admin_topic/vertifycomments}">
            <i class="fa fa-check"></i>文章评论审核
        </a>
<form  method="post">
    <table class="table">
        <tbody>
            <tr class="header" ><td colspan="4">文章列表</td></tr>
            <tr class="altbg1"><td colspan="4">可以通过如下搜索条件，检索文章</td></tr>
            <tr>
                <td width="200"  class="altbg2">标题:<input class="txt form-control" name="srchtitle" {if isset($srchtitle)}value="{$srchtitle}" {/if}></td>
                <td  width="200" class="altbg2">作者:<input class="txt form-control" name="srchauthor" {if isset($srchauthor)}value="{$srchauthor}" {/if}></td>

                 <td  width="200" class="altbg2">分类:
                    <select class="form-control shortinput" name="srchcategory" id="srchcategory"><option value="0">--不限--</option>{$catetree}</select>
                </td>
            </tr>

            <tr>
              <td  rowspan="2" class="altbg2"><input class="btn btn-info" name="submit" type="submit" value="查询"></td>
              </tr>
        </tbody>
    </table>
</form>
[共 <font color="green">{$rownum}</font> 个文章]
<form name="answerlist"  method="POST">
    <table class="table">
        <tr class="header" align="center">
            <td ><input class="checkbox" value="chkall" id="chkall" onclick="checkall('tid[]')" type="checkbox" name="chkall"><label for="chkall">选择</label></td>
               <td class="" width="3%">后台排序</td>
            <td >文章作者</td>

            <td >文章标题</td>
 <td >归属分类</td>
             <td>发布时间</td>
                <td  >阅读数</td>
            <td  >编辑</td>
        </tr>


                    <!--{loop $topiclist $topic}-->
                <tr align="center" >

                    <td  class="altbg2" style="border:none;"><input class="checkbox" type="checkbox" value="{$topic['id']}" name="tid[]"></td>
                                      <td style="border:none;" class="" width="3%"><input name="corder{$topic['id']}" type="text" value="{$topic['displayorder']}"/></td>
                                      <td style="border:none;" class="altbg2">
                                     {$topic['author']}</td>

                    <td  class="altbg2" style="border:none;"><input name="order[]" type="hidden" value="{$topic['id']}"/><strong><a target="_blank" href="{url topic/getone/$topic['id']}">
                 	{eval $topdata = $this->db->get_where ( 'topdata', array ('typeid' => $topic['id'],'type'=>'topic') )->row_array ();}
						
						
				
                 {if $topdata}<label class="label label-info">置顶</label>{/if}  {if $topic['ispc']==1}<label class="label label-danger">推荐</label>{/if} {if $topic['price']>0}<label class="label label-primary">{$topic['price']}{eval if ($topic['readmode']==2) echo '财富值'; }{eval if ($topic['readmode']==3) echo '元'; }阅读</label>{/if}{if $topic['state']==0}<label class="label label-warning">等待审核</label>{/if}{$topic['title']}
                    </a></strong></td>
                           <td style="border:none;" class="altbg2" align="center">  <a target="_blank" href="{eval echo getcaturl($topic['articleclassid'],'topic/catlist/#id#');}"> {eval echo $this->category[$topic['articleclassid']]['name']}</a></td>
                           <td style="border:none;"  class="altbg2" align="center">{$topic['viewtime']}</td>
                                  <td style="border:none;" class="altbg2" align="center">{$topic['views']}</td>
                    <td  style="border:none;" class="altbg2" align="center"><a target="_blank" href="{url admin_topic/edit/$topic['id']}">编辑</a></td>
                </tr>
                <tr><td colspan="8">
             
                <div class="row">
                <div class="col-md-4" >
                <div style="display:flex">
                  <strong>seo标题:</strong>
                 <input id="seotitle{$topic['id']}" type="text" style="margin-left:4px;border:solid 2px #2196F3;flex:1;outline:none;" placeholder="最佳25~33字内，优化页面标题，对搜索引擎友好" value="{$topic['seotitle']}" />
               
                </div>
                </div>
                <div class="col-md-4">
                <div style="display:flex">
                  <strong>seo关键词:</strong>
                 <input id="seokeyword{$topic['id']}" type="text" style="margin-left:4px;border:solid 2px #2196F3;flex:1;outline:none;" placeholder="英文逗号分开，最佳不超过6组" value="{$topic['seokeyword']}" />
               
                </div>
                </div>
                <div class="col-md-4">
                   <div style="display:flex">
                  <strong>seo描述:</strong>
                
                   <textarea id="seodescription{$topic['id']}" placeholder="最佳110字内，优化页面description关键词信息，对搜索引擎友好"  style="margin-left:4px;border:solid 2px #2196F3;flex:1;outline:none;" >{$topic['seodescription']}</textarea>
                </div>
                </div>
                <div class="col-md-12">
                <p style="font-size:12px;color:#03A9F4;font-weight:900;">seo标题，描述，关键词信息可以在文章详情页面右键查看源码，顶部title,keywords,description标签中展示</p>
                <div onclick="saveseo($topic['id'])" class="btn btn-info btn-sm">保存</div>
                </div>
                </div>
               
                </td></tr>
      
       
   <!--{/loop}-->
    </table>
   <div class="pages">{$departstr}</div>


             <input name="ctrlcase" class="btn btn-success" type="button" onClick="buttoncontrol(1);" value="推送到百度">&nbsp;&nbsp;&nbsp;
 <input name="ctrlcase" class="btn btn-success" type="button" onClick="buttoncontrol(3);" value="设置推荐/取消推荐">&nbsp;&nbsp;&nbsp;
 <input name="ctrlcase" class="btn btn-success" type="button" onClick="buttoncontrol(4);" value="顶置文章/取消顶置">&nbsp;&nbsp;&nbsp;
 <input class="button" tabindex="3" onClick="buttoncontrol(5)" type="button" value="移动分类" name="ctrlcase">&nbsp;&nbsp;&nbsp;
 <input class="button" tabindex="3" onClick="buttoncontrol(6)" type="button" value="更新排序" name="ctrlcase">&nbsp;&nbsp;&nbsp;
    <input class="button" tabindex="3" onClick="buttoncontrol(2)" type="button" value=" 删除" name="ctrlcase">

</form>
<div class="modal fade" id="movecategory">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
        <h4 class="modal-title">移动文章到指定分类</h4>
      </div>
      <div class="modal-body">
    <form name="categoryform"  action="index.php?admin_topic/movecategory{$setting['seo_suffix']}" method="post" >
        <input type="hidden" name="tids" value="" id="category_tid"/>
        <table class="table" width="470px">
            <tr>
                <td>
                    <div class="inputbox mt15">
                        <select name="category" size=1 style="width:240px" >{$catetree}</select>
                    </div>
                </td>
            </tr>
            <tr>
                <td><button type="submit" class="button flright mt15" >确定</button></td>
            </tr>
        </table>
    </form>
      </div>
   
    </div>
  </div>
</div>

<div class="modal fade" id="baidutui">
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
      <h4 class="modal-title">百度推送提醒</h4>
    </div>
    <div class="modal-body">
      <p>确定推送？此项操作只有配置了百度推送api地址有效！</p>
    </div>
    <div class="modal-footer">
     <button type="button" id="btntui" class="btn btn-primary">确定推送</button>
     <button type="button"  class="btn btn-primary" onclick="window.location.href='index.php?admin_setting/seo{$setting['seo_suffix']}'">去配置百度推送api地址</button>
      <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>

    </div>
  </div>
</div>
</div>
<br>

</div>
<!--{template footer}-->
<script>
function saveseo(_index){
	var _title=$.trim($("#seotitle"+_index).val());
	var _data={
			'id':_index,
			'seotitle':_title,
			'seokeyword':$.trim($("#seokeyword"+_index).val()),
			'seodescription':$.trim($("#seodescription"+_index).val()),
			}
	function success(result){
		console.log(result)
      alert(result.msg);
	}
	var _url="{url admin_topic/posteditseo}";
	ajaxpost(_url,_data,success);
}
function buttoncontrol(num) {

	  if ($("input[name='tid[]']:checked").length == 0) {
          alert('你没有选择任何要操作的文章！');
          return false;
      }else{
    	   switch (num) {
           case 1:
           	$("#baidutui").modal("show");

           	$("#btntui").click(function(){
           		 document.answerlist.action = "index.php?admin_topic/baidutui{$setting['seo_suffix']}";
                   console.log( document.answerlist);
           		 document.answerlist.submit();
           	})

               break;
           case 2:
        	   if (confirm('确定删除问题？该操作不可返回！') == false) {
                   return false;
               } else {
               document.answerlist.action = "index.php?admin_topic/remove{$setting['seo_suffix']}";
               document.answerlist.submit();
               }
               break;
           case 3:
        	   if (confirm('设置推荐将在推荐列表中展示，确定吗？') == false) {
                   return false;
               } else {
               document.answerlist.action = "index.php?admin_topic/recommend{$setting['seo_suffix']}";
               document.answerlist.submit();
               }
               break;
           case 4:
        	   if (confirm('置顶成功后，可在内容管理-顶置内容管理中批量操作。') == false) {
                   return false;
               } else {
               document.answerlist.action = "index.php?admin_topic/settop{$setting['seo_suffix']}";
               document.answerlist.submit();
               }
               break;
           case 5:
        	     var tids = document.getElementsByName('tid[]');
                 var num = '', tag = '';
                 for (var i = 0; i < tids.length; i++) {
                     if (tids[i].checked == true) {
                         num += tag + tids[i].value;
                         tag = ",";
                     }
                 }
                 $("#category_tid").val(num);
               $("#movecategory").modal("show");
               break;
           case 6:
        		var myurl=g_site_url+"index.php?admin_topic/updatecatbyorder{$setting['seo_suffix']}";
                document.answerlist.action = myurl;
                document.answerlist.submit();
               break;
    	   }
      }
}
{if $srchcategory}
$(document).ready(function(){
    $("#srchcategory option").each(function(){
        if($(this).val()==$srchcategory){
            $(this).prop("selected","true");
        }
    });
});
{/if}
</script>